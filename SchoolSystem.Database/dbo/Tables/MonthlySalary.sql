﻿CREATE TABLE [dbo].[MonthlySalary] (
    [Id]                INT        IDENTITY (1, 1) NOT NULL,
    [DocNo]             NCHAR (10) NULL,
    [Date]              DATE       NULL,
    [BranchId]          INT        NULL,
    [EducationalYearId] INT        NULL,
    [EmployeeId]        INT        NULL,
    [Month]             INT        NULL,
    [Year]              INT        NULL,
    [NetSalary]         MONEY      NULL,
    CONSTRAINT [PK_MonthlySalary] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MonthlySalary_EducationYear] FOREIGN KEY ([EducationalYearId]) REFERENCES [dbo].[EducationYear] ([Id]),
    CONSTRAINT [FK_MonthlySalary_RegistrationEmployee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[RegistrationEmployee] ([Id])
);

