﻿CREATE TABLE [dbo].[PayrollData] (
    [Id]            INT   IDENTITY (1, 1) NOT NULL,
    [EmployeeId]    INT   NULL,
    [PayrollItemId] INT   NULL,
    [Value]         MONEY NULL,
    CONSTRAINT [PK_PayrollData] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PayrollData_PayrollItem] FOREIGN KEY ([PayrollItemId]) REFERENCES [dbo].[PayrollItem] ([Id]),
    CONSTRAINT [FK_PayrollData_RegistrationEmployee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[RegistrationEmployee] ([Id])
);

