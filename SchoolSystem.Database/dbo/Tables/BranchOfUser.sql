﻿CREATE TABLE [dbo].[BranchOfUser] (
    [BranchId] INT            NOT NULL,
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [UserId]   NVARCHAR (128) NULL,
    CONSTRAINT [PK_BranchOfUser_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_BranchOfUser_AspNetUsers] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_BranchOfUser_Branch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id])
);



