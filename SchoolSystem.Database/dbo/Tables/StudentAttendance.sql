﻿CREATE TABLE [dbo].[StudentAttendance] (
    [Id]                  INT  IDENTITY (1, 1) NOT NULL,
    [date]                DATE NULL,
    [Branch_Id]           INT  NULL,
    [EducationalYear_Id]  INT  NULL,
    [Class_Id]            INT  NULL,
    [Stage_Id]            INT  NULL,
    [IsDeleted]           BIT  CONSTRAINT [DF_StudentAttendance_IsDeleted] DEFAULT ((0)) NOT NULL,
    [EducationalGrade_Id] INT  NULL,
    CONSTRAINT [PK_StudentAttendance] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StudentAttendance_Branch] FOREIGN KEY ([Branch_Id]) REFERENCES [dbo].[Branch] ([Id]),
    CONSTRAINT [FK_StudentAttendance_Class] FOREIGN KEY ([Class_Id]) REFERENCES [dbo].[Class] ([Id]),
    CONSTRAINT [FK_StudentAttendance_EducationalGrade] FOREIGN KEY ([EducationalGrade_Id]) REFERENCES [dbo].[EducationalGrade] ([Id]),
    CONSTRAINT [FK_StudentAttendance_EducationYear] FOREIGN KEY ([EducationalYear_Id]) REFERENCES [dbo].[EducationYear] ([Id]),
    CONSTRAINT [FK_StudentAttendance_Stage] FOREIGN KEY ([Stage_Id]) REFERENCES [dbo].[Stage] ([Id])
);

