﻿CREATE TABLE [dbo].[Major] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]          NVARCHAR (50) NULL,
    [NameEn]          NVARCHAR (50) NULL,
    [QualificationId] INT           NOT NULL,
    [IsDeleted]       BIT           NULL,
    CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Major_Qualification] FOREIGN KEY ([QualificationId]) REFERENCES [dbo].[Qualification] ([Id])
);

