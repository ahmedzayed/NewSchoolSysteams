﻿CREATE TABLE [dbo].[EmployeeAttendanceDetail] (
    [Id]                   INT IDENTITY (1, 1) NOT NULL,
    [EmployeeAttendanceId] INT NULL,
    [EmployeeId]           INT NULL,
    [Attended]             BIT NULL,
    [IsDeleted]            BIT NULL,
    CONSTRAINT [PK_EmployeeAttendanceDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EmployeeAttendanceDetail_EmployeeAttendance] FOREIGN KEY ([EmployeeAttendanceId]) REFERENCES [dbo].[EmployeeAttendance] ([Id]),
    CONSTRAINT [FK_EmployeeAttendanceDetail_RegistrationEmployee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[RegistrationEmployee] ([Id])
);

