﻿CREATE TABLE [dbo].[SchoolExpenses] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [DocNo]       NVARCHAR (100) NULL,
    [ExpenseDate] DATETIME       NULL,
    [ItemName]    NVARCHAR (100) NULL,
    [DeliveredTo] NVARCHAR (100) NULL,
    [Quantity]    INT            NULL,
    [Price]       DECIMAL (18)   NULL,
    [Total]       DECIMAL (18)   NULL,
    [IsDeleted]   BIT            NULL,
    CONSTRAINT [PK_SchoolExpenses_1] PRIMARY KEY CLUSTERED ([Id] ASC)
);

