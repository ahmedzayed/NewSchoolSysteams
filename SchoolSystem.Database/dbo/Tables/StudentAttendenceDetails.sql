﻿CREATE TABLE [dbo].[StudentAttendenceDetails] (
    [Id]                   INT IDENTITY (1, 1) NOT NULL,
    [StudentAttendence_Id] INT NULL,
    [Student_Id]           INT NULL,
    [Attended]             BIT NULL,
    [IsDeleted]            BIT CONSTRAINT [DF_StudentAttendenceDetails_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_StudentAttendenceDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StudentAttendenceDetails_RegistrationStudent] FOREIGN KEY ([Student_Id]) REFERENCES [dbo].[RegistrationStudent] ([Id]),
    CONSTRAINT [FK_StudentAttendenceDetails_StudentAttendance] FOREIGN KEY ([StudentAttendence_Id]) REFERENCES [dbo].[StudentAttendance] ([Id])
);

