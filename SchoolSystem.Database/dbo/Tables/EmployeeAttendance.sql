﻿CREATE TABLE [dbo].[EmployeeAttendance] (
    [Id]                INT  IDENTITY (1, 1) NOT NULL,
    [Date]              DATE NULL,
    [BranchId]          INT  NOT NULL,
    [EducationalYearId] INT  NULL,
    [IsDeleted]         BIT  NULL,
    CONSTRAINT [PK_EmployeeAttendance] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EmployeeAttendance_Branch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id]),
    CONSTRAINT [FK_EmployeeAttendance_EducationYear] FOREIGN KEY ([EducationalYearId]) REFERENCES [dbo].[EducationYear] ([Id])
);

