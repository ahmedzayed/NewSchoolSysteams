﻿CREATE TABLE [dbo].[AspNetRoles] (
    [Id]       NVARCHAR (128) NOT NULL,
    [Name]     NVARCHAR (256) NOT NULL,
    [GroupsId] INT            NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AspNetRoles_Groups] FOREIGN KEY ([GroupsId]) REFERENCES [dbo].[Groups] ([Id])
);

