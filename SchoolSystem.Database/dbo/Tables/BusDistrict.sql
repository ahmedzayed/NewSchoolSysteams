﻿CREATE TABLE [dbo].[BusDistrict] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [BusId]      INT NULL,
    [DistrictId] INT NULL,
    [IsDeleted]  BIT NULL,
    [BranchId]   INT NULL,
    CONSTRAINT [PK_BusDistrict] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_BusDistrict_Branch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id]),
    CONSTRAINT [FK_BusDistrict_Bus] FOREIGN KEY ([BusId]) REFERENCES [dbo].[Bus] ([Id]),
    CONSTRAINT [FK_BusDistrict_District] FOREIGN KEY ([DistrictId]) REFERENCES [dbo].[District] ([Id])
);



