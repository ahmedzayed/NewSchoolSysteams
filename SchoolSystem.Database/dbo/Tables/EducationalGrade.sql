﻿CREATE TABLE [dbo].[EducationalGrade] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]          NVARCHAR (50) NULL,
    [NameEn]          NVARCHAR (50) NULL,
    [BranchId]        INT           NULL,
    [EducationYearId] INT           NULL,
    [StageId]         INT           NULL,
    [IsDeleted]       BIT           NULL,
    CONSTRAINT [PK_EducationalGrade] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EducationalGrade_EducationYear] FOREIGN KEY ([EducationYearId]) REFERENCES [dbo].[EducationYear] ([Id]),
    CONSTRAINT [FK_EducationalGrade_Stage] FOREIGN KEY ([StageId]) REFERENCES [dbo].[Stage] ([Id])
);

