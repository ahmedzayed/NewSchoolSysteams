﻿CREATE TABLE [dbo].[SchoolUser] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [UserName]    NVARCHAR (50)  NOT NULL,
    [Password]    NVARCHAR (128) NULL,
    [RoleId]      INT            NULL,
    [SystemAdmin] BIT            NULL,
    [IsDeleted]   BIT            NULL,
    CONSTRAINT [PK_SchoolUser] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SchoolUser_SchoolRole] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[SchoolRole] ([Id]),
    CONSTRAINT [IX_SchoolUser] UNIQUE NONCLUSTERED ([UserName] ASC)
);

