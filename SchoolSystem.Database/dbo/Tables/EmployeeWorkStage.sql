﻿CREATE TABLE [dbo].[EmployeeWorkStage] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [EmployeeId] INT NULL,
    [StageId]    INT NULL,
    CONSTRAINT [PK_EmployeeWorkStage] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EmployeeWorkStage_RegistrationEmployee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[RegistrationEmployee] ([Id]),
    CONSTRAINT [FK_EmployeeWorkStage_Stage] FOREIGN KEY ([StageId]) REFERENCES [dbo].[Stage] ([Id])
);

