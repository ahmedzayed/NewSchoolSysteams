﻿CREATE TABLE [dbo].[BusSubscriptionFee] (
    [Id]               INT     IDENTITY (1, 1) NOT NULL,
    [EducationYearId]  INT     NULL,
    [BranchId]         INT     NULL,
    [SubscriptionType] TINYINT NULL,
    [Fees]             MONEY   NULL,
    [IsDeleted]        BIT     NULL,
    CONSTRAINT [PK_BusSubscriptionFees] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0=ذهاب
1= عودة
2=ذهاب و عودة', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BusSubscriptionFee', @level2type = N'COLUMN', @level2name = N'SubscriptionType';

