﻿CREATE TABLE [dbo].[AppActions] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [ActionName]   VARCHAR (50)   NOT NULL,
    [ControllerId] INT            NOT NULL,
    [Description]  NVARCHAR (150) NULL,
    CONSTRAINT [PK_AppActions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AppActions_AppControllers] FOREIGN KEY ([ControllerId]) REFERENCES [dbo].[AppControllers] ([Id])
);

