﻿CREATE TABLE [dbo].[PayrollItem] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]    NVARCHAR (50) NULL,
    [NameEn]    NVARCHAR (50) NULL,
    [Type]      TINYINT       NULL,
    [Value]     MONEY         NULL,
    [IsDeleted] BIT           NULL,
    CONSTRAINT [PK_PayrollItem] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N' (zero means  مستحق  and one means  مستقطع)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PayrollItem', @level2type = N'COLUMN', @level2name = N'IsDeleted';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'(zero means  مستحق  and one means  مستقطع)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PayrollItem', @level2type = N'COLUMN', @level2name = N'Type';

