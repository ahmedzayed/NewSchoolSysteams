﻿CREATE TABLE [dbo].[RolesGroups] (
    [Groups_Id] INT NOT NULL,
    [Roles_Id]  INT NOT NULL,
    CONSTRAINT [PK_RolesGroups] PRIMARY KEY CLUSTERED ([Groups_Id] ASC, [Roles_Id] ASC),
    CONSTRAINT [FK_RolesGroups_Roles] FOREIGN KEY ([Roles_Id]) REFERENCES [dbo].[AppActions] ([Id])
);

