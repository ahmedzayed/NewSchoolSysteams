﻿CREATE TABLE [dbo].[Driver] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]          NVARCHAR (50) NULL,
    [NameEn]          NVARCHAR (50) NULL,
    [BranchId]        INT           NOT NULL,
    [EducationYearId] INT           NOT NULL,
    [IsDeleted]       BIT           NULL,
    CONSTRAINT [PK_Driver] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Driver_Branch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id]),
    CONSTRAINT [FK_Driver_EducationYear] FOREIGN KEY ([EducationYearId]) REFERENCES [dbo].[EducationYear] ([Id])
);

