﻿CREATE TABLE [dbo].[EducationFee] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [EducationFeesAmount]    DECIMAL (18, 2) NULL,
    [RegistrationFeesAmount] DECIMAL (18, 2) NULL,
    [BranchId]               INT             NULL,
    [EducationYearId]        INT             NULL,
    [IsDeleted]              BIT             NULL,
    [EducationalGradeId]     INT             NULL,
    CONSTRAINT [PK_EducationFees] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EducationFee_EducationalGrade] FOREIGN KEY ([EducationalGradeId]) REFERENCES [dbo].[EducationalGrade] ([Id]),
    CONSTRAINT [FK_EducationFees_Branch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id]),
    CONSTRAINT [FK_EducationFees_EducationYear] FOREIGN KEY ([EducationYearId]) REFERENCES [dbo].[EducationYear] ([Id])
);



