﻿CREATE TABLE [dbo].[StudentRecoveryBatch] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [DocNo]           NVARCHAR (50)   NULL,
    [VoucherDate]     DATETIME        NULL,
    [BranchId]        INT             NULL,
    [EducationYearId] INT             NULL,
    [StudentId]       INT             NULL,
    [RecoveryValue]   MONEY           NULL,
    [VAT]             MONEY           NULL,
    [Notes]           NVARCHAR (3000) NULL,
    [IsDeleted]       BIT             NULL,
    CONSTRAINT [PK_StudentRecoveryBatch] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StudentRecoveryBatch_EducationYear] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[EducationYear] ([Id]),
    CONSTRAINT [FK_StudentRecoveryBatch_RegistrationStudent] FOREIGN KEY ([StudentId]) REFERENCES [dbo].[RegistrationStudent] ([Id])
);

