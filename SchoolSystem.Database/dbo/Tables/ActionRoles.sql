﻿CREATE TABLE [dbo].[ActionRoles] (
    [AppActions_Id] INT NOT NULL,
    [RolesId]       INT NULL,
    CONSTRAINT [FK_ActionRoles_AppActions] FOREIGN KEY ([AppActions_Id]) REFERENCES [dbo].[AppActions] ([Id]),
    CONSTRAINT [FK_ActionRoles_Roles] FOREIGN KEY ([RolesId]) REFERENCES [dbo].[Roles] ([Id])
);

