﻿CREATE TABLE [dbo].[BusSubscriptionFeesType] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]    NVARCHAR (50) NULL,
    [NameEn]    NVARCHAR (50) NULL,
    [IsDeleted] BIT           NOT NULL,
    [Value]     MONEY         NULL,
    CONSTRAINT [PK_BusSubscriptionFeesType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

