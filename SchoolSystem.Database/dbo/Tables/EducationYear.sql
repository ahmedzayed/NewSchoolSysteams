﻿CREATE TABLE [dbo].[EducationYear] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]    NVARCHAR (50) NULL,
    [NameEn]    NVARCHAR (50) NULL,
    [IsDeleted] BIT           NULL,
    [IsDefault] BIT           NULL,
    [StartDate] DATETIME      NULL,
    [EndDate]   DATETIME      NULL,
    CONSTRAINT [PK_EducationYear] PRIMARY KEY CLUSTERED ([Id] ASC)
);



