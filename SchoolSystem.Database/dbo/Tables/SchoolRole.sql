﻿CREATE TABLE [dbo].[SchoolRole] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [RoleName]  NVARCHAR (50) NULL,
    [IsDeleted] BIT           NULL,
    CONSTRAINT [PK_SchoolRole] PRIMARY KEY CLUSTERED ([Id] ASC)
);

