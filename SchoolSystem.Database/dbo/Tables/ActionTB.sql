﻿CREATE TABLE [dbo].[ActionTB] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [ActionName]       NVARCHAR (50) NULL,
    [UserActionArName] NVARCHAR (50) NULL,
    [UserActionEnName] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Action] PRIMARY KEY CLUSTERED ([Id] ASC)
);

