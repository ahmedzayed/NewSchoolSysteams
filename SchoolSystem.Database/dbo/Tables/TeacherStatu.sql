﻿CREATE TABLE [dbo].[TeacherStatu] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]    NVARCHAR (50) NULL,
    [NameEn]    NVARCHAR (50) NULL,
    [IsDeleted] BIT           NULL,
    CONSTRAINT [PK_TeacherStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

