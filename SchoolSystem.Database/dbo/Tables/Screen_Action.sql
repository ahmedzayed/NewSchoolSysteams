﻿CREATE TABLE [dbo].[Screen_Action] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [ActionId] INT NULL,
    [ScreenId] INT NULL,
    CONSTRAINT [PK_Screen_Action] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Screen_Action_Action] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[ActionTB] ([Id]),
    CONSTRAINT [FK_Screen_Action_Screen] FOREIGN KEY ([ScreenId]) REFERENCES [dbo].[Screen] ([Id])
);

