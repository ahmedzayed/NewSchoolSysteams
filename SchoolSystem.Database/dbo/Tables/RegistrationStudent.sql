﻿CREATE TABLE [dbo].[RegistrationStudent] (
    [Id]                        INT             IDENTITY (1, 1) NOT NULL,
    [StuNameAr]                 NVARCHAR (50)   NULL,
    [StuNameEn]                 NVARCHAR (50)   NULL,
    [ParentNameAr]              NVARCHAR (50)   NULL,
    [ParentNameEn]              NVARCHAR (50)   NULL,
    [GrandfatherNameAr]         NVARCHAR (50)   NULL,
    [GrandfatherNameEn]         NVARCHAR (50)   NULL,
    [FamilyNameAr]              NVARCHAR (50)   NULL,
    [FamilyNameEn]              NVARCHAR (50)   NULL,
    [Address]                   NVARCHAR (MAX)  NULL,
    [Notes]                     NVARCHAR (MAX)  NULL,
    [IsDeleted]                 BIT             NULL,
    [RegisterFees]              MONEY           NULL,
    [StudyFees]                 MONEY           NULL,
    [AdditionalFees]            MONEY           NULL,
    [DiscountPercentage]        FLOAT (53)      NULL,
    [RecordNumber]              NVARCHAR (50)   NULL,
    [EnrollmentStatus]          INT             NULL,
    [TransportationFees]        DECIMAL (18, 2) NULL,
    [PreviousFees]              DECIMAL (18, 2) NULL,
    [Type]                      INT             NULL,
    [FatherPhone]               NVARCHAR (50)   NULL,
    [MotherPhone]               NVARCHAR (50)   NULL,
    [BranchId]                  INT             NULL,
    [EducationYearId]           INT             NULL,
    [ClassId]                   INT             NULL,
    [BusId]                     INT             NULL,
    [NationalityId]             INT             NULL,
    [StageId]                   INT             NULL,
    [EducationalGradeId]        INT             NULL,
    [DistrictId]                INT             NULL,
    [HijriRegistrationDate]     NVARCHAR (50)   NULL,
    [MiladiRegistrationDate]    DATETIME        NULL,
    [HijriBirthDate]            NVARCHAR (50)   NULL,
    [MiladiBirthDate]           DATETIME        NULL,
    [Image1]                    NVARCHAR (50)   NULL,
    [Image2]                    NVARCHAR (50)   NULL,
    [Image3]                    NVARCHAR (50)   NULL,
    [Image4]                    NVARCHAR (50)   NULL,
    [Image5]                    NVARCHAR (50)   NULL,
    [DescroptionImage1]         NVARCHAR (50)   NULL,
    [DescroptionImage2]         NVARCHAR (50)   NULL,
    [DescroptionImage3]         NVARCHAR (50)   NULL,
    [DescroptionImage4]         NVARCHAR (50)   NULL,
    [DescroptionImage5]         NVARCHAR (50)   NULL,
    [BusSubscriptionFeesTypeId] INT             NULL,
    [Total]                     MONEY           NULL,
    CONSTRAINT [PK_RegistrationStudent] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RegistrationStudent_Branch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[Branch] ([Id]),
    CONSTRAINT [FK_RegistrationStudent_Bus] FOREIGN KEY ([BusId]) REFERENCES [dbo].[Bus] ([Id]),
    CONSTRAINT [FK_RegistrationStudent_BusSubscriptionFeesType] FOREIGN KEY ([BusSubscriptionFeesTypeId]) REFERENCES [dbo].[BusSubscriptionFeesType] ([Id]),
    CONSTRAINT [FK_RegistrationStudent_Class] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[Class] ([Id]),
    CONSTRAINT [FK_RegistrationStudent_District] FOREIGN KEY ([DistrictId]) REFERENCES [dbo].[District] ([Id]),
    CONSTRAINT [FK_RegistrationStudent_EducationalGrade] FOREIGN KEY ([EducationalGradeId]) REFERENCES [dbo].[EducationalGrade] ([Id]),
    CONSTRAINT [FK_RegistrationStudent_Nationality] FOREIGN KEY ([NationalityId]) REFERENCES [dbo].[Nationality] ([Id]),
    CONSTRAINT [FK_RegistrationStudent_Stage] FOREIGN KEY ([EducationalGradeId]) REFERENCES [dbo].[Stage] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0ذكر,1انثي', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegistrationStudent', @level2type = N'COLUMN', @level2name = N'Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0مستجد,1مرفع,2مفصول', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegistrationStudent', @level2type = N'COLUMN', @level2name = N'EnrollmentStatus';

