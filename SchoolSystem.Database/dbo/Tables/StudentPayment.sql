﻿CREATE TABLE [dbo].[StudentPayment] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [DocNo]           NVARCHAR (50)   NULL,
    [VoucherDate]     DATETIME        NULL,
    [BranchId]        INT             NULL,
    [EducationYearId] INT             NULL,
    [StudentId]       INT             NULL,
    [PaymentValue]    MONEY           NULL,
    [VAT]             MONEY           NULL,
    [Notes]           NVARCHAR (3000) NULL,
    [IsDeleted]       BIT             NULL,
    CONSTRAINT [PK_StudentPayment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [IX_StudentPayment] UNIQUE NONCLUSTERED ([DocNo] ASC)
);

