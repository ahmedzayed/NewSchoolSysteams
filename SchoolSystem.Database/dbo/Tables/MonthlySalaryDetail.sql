﻿CREATE TABLE [dbo].[MonthlySalaryDetail] (
    [Id]              INT   IDENTITY (1, 1) NOT NULL,
    [MonthlySalaryId] INT   NULL,
    [PayrollItemId]   INT   NULL,
    [Value]           MONEY NULL,
    CONSTRAINT [PK_MonthlySalaryDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MonthlySalaryDetail_MonthlySalary] FOREIGN KEY ([MonthlySalaryId]) REFERENCES [dbo].[MonthlySalary] ([Id])
);

