﻿CREATE TABLE [dbo].[Class] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [ClassName]          NVARCHAR (50) NULL,
    [EducationYearId]    INT           NULL,
    [BranchId]           INT           NULL,
    [EducationalGradeId] INT           NULL,
    [StudentCount]       INT           NULL,
    [IsDeleted]          BIT           NULL,
    CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Class_EducationalGrade] FOREIGN KEY ([EducationalGradeId]) REFERENCES [dbo].[EducationalGrade] ([Id])
);

