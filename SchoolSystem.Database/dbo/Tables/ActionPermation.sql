﻿CREATE TABLE [dbo].[ActionPermation] (
    [Id]              INT IDENTITY (1, 1) NOT NULL,
    [UserId]          INT NOT NULL,
    [Screen_ActionID] INT NOT NULL,
    [IsEnable]        BIT NULL,
    CONSTRAINT [PK_ActionPermation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ActionPermation_Screen_Action] FOREIGN KEY ([Screen_ActionID]) REFERENCES [dbo].[Screen_Action] ([Id])
);

