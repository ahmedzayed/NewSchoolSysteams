﻿CREATE TABLE [dbo].[Job] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [NameAr]    NVARCHAR (50) NULL,
    [NameEn]    NVARCHAR (50) NULL,
    [IsDeleted] BIT           NULL,
    CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED ([Id] ASC)
);

