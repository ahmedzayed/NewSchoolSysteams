﻿CREATE TABLE [dbo].[Bus] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [BusName]         NVARCHAR (50) NULL,
    [BusNo]           NVARCHAR (50) NULL,
    [Capacity]        INT           NULL,
    [EducationYearId] INT           NULL,
    [BranchId]        INT           NULL,
    [DriverId]        INT           NULL,
    [IsDeleted]       BIT           NULL,
    CONSTRAINT [PK_Bus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

