﻿CREATE TABLE [dbo].[AppControllers] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Area]           VARCHAR (50)  NULL,
    [ControllerName] VARCHAR (50)  NOT NULL,
    [ControllerPath] VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_AppControllers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

