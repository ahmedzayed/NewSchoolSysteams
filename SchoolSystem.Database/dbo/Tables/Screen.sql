﻿CREATE TABLE [dbo].[Screen] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [ScreenName]       NVARCHAR (50) NULL,
    [UserScreenArName] NVARCHAR (50) NULL,
    [UserScreenEnName] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Screen] PRIMARY KEY CLUSTERED ([Id] ASC)
);

