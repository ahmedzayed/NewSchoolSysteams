﻿Create function [dbo].[F_SumMostaktaEmployee](@branchid int ,@eduid int, @month int ,@year int,@empId int)
returns decimal(18,2)
as
begin
declare @ret decimal(18,2)
select @ret= isnull(sum([MonthlySalaryDetail].[Value]),0) from [dbo].[MonthlySalaryDetail]
join [dbo].[PayrollItem] on [dbo].[PayrollItem].Id=[MonthlySalaryDetail].[PayrollItemId]
join [dbo].[MonthlySalary] s on s.Id=[MonthlySalaryDetail].[MonthlySalaryId]
where [dbo].[PayrollItem].[Type]=1
and [dbo].[PayrollItem].[IsDeleted] = 0
and s.[BranchId]= @branchid and s.[EducationalYearId]=@eduid and [Month]=@month and [Year]=@year
and s.[EmployeeId]=@empId
return @ret
end