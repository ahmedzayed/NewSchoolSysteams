﻿create function [dbo].[UDF_SumStudentRefunds](@BranchId int,@EducationYearId int,@StudentId int)
returns decimal(18,2)
as
begin
declare @ret decimal(18,2)
select @ret = isnull(sum([RecoveryValue]),0) + isnull(sum([VAT]),0) from [dbo].[StudentRecoveryBatch]
where [StudentId] = @studentId 
and [BranchId] = @BranchId
and [EducationYearId] = @EducationYearId
and [IsDeleted] = 0

return @ret
end