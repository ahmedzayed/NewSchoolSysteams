﻿CREATE Proc [dbo].[SP_AddEducationYea]
@NameAr nvarchar(50),
@NameEn nvarchar(50),
@StartDate Datetime,
@EndDate dateTime ,
@IsDeleted bit,
@IsDefault bit ,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[EducationYear] (NameAr,NameEn,StartDate,EndDate,IsDefault,IsDeleted)  values(@NameAr,@NameEn,@StartDate,@EndDate,
@IsDefault,0)  
end
else
begin
update[dbo].[EducationYear] set NameAr=@NameAr,
NameEn=@NameEn,
StartDate=@StartDate,
EndDate=@EndDate,
IsDefault=@IsDefault,
IsDeleted=0

where Id=@Id
end