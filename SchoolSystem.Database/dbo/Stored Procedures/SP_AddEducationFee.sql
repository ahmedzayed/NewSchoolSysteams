﻿CREATE Proc [dbo].[SP_AddEducationFee]
@EducationYearId int,
@BranchId int ,
@EducationalGradeId int ,
@EducationFeesAmount dec,
@RegistrationFeesAmount dec,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[EducationFee] (EducationYearId ,BranchId,EducationalGradeId,EducationFeesAmount,RegistrationFeesAmount) 
 values(@EducationYearId,@BranchId,@EducationalGradeId,@EducationFeesAmount,@RegistrationFeesAmount )  
end
else
begin
update[dbo].[EducationFee] set EducationYearId=@EducationYearId,
BranchId=@BranchId,
EducationalGradeId=@EducationalGradeId,
EducationFeesAmount=@EducationFeesAmount,
RegistrationFeesAmount=@RegistrationFeesAmount

where Id=@Id
end