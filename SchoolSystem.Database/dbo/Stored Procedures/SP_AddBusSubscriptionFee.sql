﻿Create Proc [dbo].[SP_AddBusSubscriptionFee]
@BranchId int ,
@EducationYearId int ,

@Fees int,
@SubscriptionType int,
@IsDeleted bit ,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[BusSubscriptionFee] (BranchId,EducationYearId,Fees,SubscriptionType,IsDeleted)  values(@BranchId,@EducationYearId,@Fees
,@SubscriptionType,@IsDeleted)  
end
else
begin
update[dbo].[BusSubscriptionFee] set 
EducationYearId=@EducationYearId,
BranchId=@BranchId,
Fees=@Fees,
SubscriptionType=@SubscriptionType,
IsDeleted=@IsDeleted

where Id=@Id
end