﻿Create Proc [dbo].[SP_AddDriver]
@NameAr nvarchar(50),
@NameEn nvarchar(50),
@BranchId int ,
@YearId int ,
@IsDeleted bit,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[Driver] (NameAr,NameEn,BranchId,EducationYearId,IsDeleted)  values(@NameAr,@NameEn,@BranchId,@YearId,@IsDeleted)  
end
else
begin
update[dbo].[Driver] set NameAr=@NameAr,
NameEn=@NameEn,
BranchId=@BranchId,
EducationYearId=@YearId,
IsDeleted=@IsDeleted

where Id=@Id
end