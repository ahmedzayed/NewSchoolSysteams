﻿Create Proc [dbo].[GetBranches]
as
begin
select 
b.Id,
a.NameAr as BranchName,
u.UserName as UserName
 From BranchOfUser b join Branch a on b.BranchId=a.Id join AspNetUsers u on b.UserId=u.Id
end