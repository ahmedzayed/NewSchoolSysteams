﻿CREATE Proc [dbo].[SP_AddClass]
@ClassName nvarchar(50),
@BranchId int ,
@YearId int ,



@EducationalGradeId int,
@StudentCount int ,
@IsDeleted bit ,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[Class] (ClassName,BranchId,EducationYearId,EducationalGradeId,StudentCount,IsDeleted)  values(@ClassName,@BranchId,@YearId
,@EducationalGradeId,@StudentCount,@IsDeleted)  
end
else
begin
update[dbo].[Class] set ClassName=@ClassName,
BranchId=@BranchId,
EducationYearId=@YearId,

EducationalGradeId=@EducationalGradeId,
StudentCount=@StudentCount,
IsDeleted=@IsDeleted

where Id=@Id
end