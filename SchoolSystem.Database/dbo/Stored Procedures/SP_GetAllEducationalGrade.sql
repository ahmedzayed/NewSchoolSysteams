﻿Create Proc [dbo].[SP_GetAllEducationalGrade]
@BranchId int
as
begin
select 
p.Id,
p.NameAr,
p.NameEn,
b.NameAr as BranchName,
e.NameAr as EducationYearName ,
s.NameAr as StageName,
p.IsDeleted

 From EducationalGrade p join Branch b on p.BranchId=b.Id join EducationYear e on p.EducationYearId=e.Id join Stage s on p.StageId=s.Id where p.BranchId=@BranchId
end