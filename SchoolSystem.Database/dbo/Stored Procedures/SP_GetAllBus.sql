﻿CREATE Proc [dbo].[SP_GetAllBus]
@BranchId int 
as
begin
select 
p.Id,
p.BusName,
p.BusNo,
p.Capacity,
e.NameAr as EducationYearName,
b.NameAr as BranchName ,
d.NameAr as DriverName 

 From Bus  p join EducationYear e on p.EducationYearId=e.Id join Branch b on p.BranchId=b.Id join Driver d on p.DriverId=d.Id where   p.BranchId=@BranchId
end