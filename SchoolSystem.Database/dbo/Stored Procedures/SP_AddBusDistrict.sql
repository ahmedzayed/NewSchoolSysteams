﻿Create Proc [dbo].[SP_AddBusDistrict]

@BusId int,
@DistrictId int ,
@BranchId int ,
@IsDeleted bit,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[BusDistrict] (BusId,DistrictId,BranchId,IsDeleted)  values(@BusId,@DistrictId,@BranchId,@IsDeleted)  
end
else
begin
update[dbo].[BusDistrict] set BusId=@BusId,
DistrictId=@DistrictId,
BranchId=@BranchId,
IsDeleted=@IsDeleted

where Id=@Id
end