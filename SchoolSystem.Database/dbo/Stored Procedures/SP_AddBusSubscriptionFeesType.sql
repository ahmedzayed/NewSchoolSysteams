﻿CREATE Proc [dbo].[SP_AddBusSubscriptionFeesType]
@NameAr nvarchar(50),
@NameEn nvarchar(50),
@Value dec ,
@IsDeleted bit,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[BusSubscriptionFeesType] (NameAr,NameEn,Value,IsDeleted)  values(@NameAr,@NameEn,@Value,@IsDeleted)  
end
else
begin
update[dbo].[BusSubscriptionFeesType] set NameAr=@NameAr,
NameEn=@NameEn,
Value=@Value,
IsDeleted=@IsDeleted

where Id=@Id
end