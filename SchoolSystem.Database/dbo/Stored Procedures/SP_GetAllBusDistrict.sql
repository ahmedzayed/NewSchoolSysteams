﻿CREATE Proc [dbo].[SP_GetAllBusDistrict]
@BranchId int
as
begin
select 
p.Id,
b.BusName as BusName,
d.NameAr as DistrictName ,
r.NameAr as BranchName,
p.IsDeleted

 From BusDistrict p join Bus b on p.BusId=b.Id join District d on p.DistrictId=d.Id join Branch r on p.BranchId=r.Id where p.BranchId=@BranchId
end