﻿Create Proc [dbo].[SP_GetAllClass]
@BranchId int 
as
begin
select 
p.Id,
p.ClassName,
p.StudentCount,

e.NameAr as EducationYearName,
b.NameAr as BranchName ,
d.NameAr as EducationalGradeName ,
p.IsDeleted

 From Class  p join EducationYear e on p.EducationYearId=e.Id join Branch b on p.BranchId=b.Id join EducationalGrade d on p.EducationalGradeId=d.Id where   p.BranchId=@BranchId
end