﻿CREATE Proc [dbo].[SP_SelectBranchesOfUser]
@Id nvarchar(128) 
as
begin
select 
p.Id,
a.NameAr as BranchName,
u.UserName as UserName
 from BranchOfUser p join Branch a on p.BranchId=a.Id join AspNetUsers u on p.UserId=u.Id where p.UserId=@Id
end