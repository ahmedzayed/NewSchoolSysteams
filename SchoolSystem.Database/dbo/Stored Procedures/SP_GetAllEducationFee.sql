﻿CREATE Proc [dbo].[SP_GetAllEducationFee]
@BranchId int 
as
begin
select 
m.Id,
b.NameAr as BranchName,
y.NameAr as EducationYearName ,
s.NameAr as EducationGardnName,
m.EducationFeesAmount,
m.RegistrationFeesAmount,
m.IsDeleted

 From  EducationFee m join Branch b on m.BranchId=b.Id join EducationYear y on m.EducationYearId=y.Id join EducationalGrade s on m.EducationalGradeId=s.Id where m.BranchId=@BranchId
end