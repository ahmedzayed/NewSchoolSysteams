﻿CREATE Proc [dbo].[SP_AddEducationalGrade]
@NameAr nvarchar(50) ,
@NameEn nvarchar(50) ,
@StageId int ,
@BranchId int,
@YearId int,
@IsDeleted bit ,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[EducationalGrade] (NameAr,NameEn,StageId, BranchId,EducationYearId,IsDeleted)  values(@NameAr,@NameEn,@StageId,@BranchId
,@YearId,@IsDeleted)  
end
else
begin
update[dbo].[EducationalGrade] set 
NameAr=@NameAr,
NameEn=@NameEn,
StageId=@StageId,
BranchId=@BranchId,
EducationYearId=@YearId,
IsDeleted=@IsDeleted

where Id=@Id
end