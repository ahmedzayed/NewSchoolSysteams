﻿Create Proc [dbo].[SP_GetAllBusSubscriptionFee]
@BranchId int 
as
begin
select 
p.Id,
p.Fees,
e.NameAr as EducationYearName,
b.NameAr as BranchName ,
d.NameAr as SubscriptionTypeName ,
p.IsDeleted

 From BusSubscriptionFee  p join EducationYear e on p.EducationYearId=e.Id join Branch b on p.BranchId=b.Id join BusSubscriptionFeesType d on p.SubscriptionType=d.Id where   p.BranchId=@BranchId
end