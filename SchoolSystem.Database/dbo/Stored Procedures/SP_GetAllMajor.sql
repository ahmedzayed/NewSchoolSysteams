﻿CREATE Proc [dbo].[SP_GetAllMajor]
as
begin
select 
m.Id,
m.NameAr,
m.NameEn ,
q.NameAr as QualificationName,
m.IsDeleted

 From  Major m join Qualification q on m.QualificationId=q.Id
end