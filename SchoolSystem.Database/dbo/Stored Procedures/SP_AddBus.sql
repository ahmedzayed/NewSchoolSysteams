﻿Create Proc [dbo].[SP_AddBus]
@BusName nvarchar(50),
@BusNo nvarchar(50),
@Capacity int ,
@EducationYearId int ,
@BranchId int ,
@DriverId int,
@IsDeleted bit ,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[Bus] (BusName,BusNo,Capacity,EducationYearId,BranchId,DriverId,IsDeleted)  values(@BusName,@BusNo,@Capacity
,@EducationYearId,@BranchId,@DriverId,@IsDeleted)  
end
else
begin
update[dbo].[Bus] set BusName=@BusName,
BusNo=@BusNo,
Capacity=@Capacity,
EducationYearId=@EducationYearId,
BranchId=@BranchId,
DriverId=@DriverId,
IsDeleted=@IsDeleted

where Id=@Id
end