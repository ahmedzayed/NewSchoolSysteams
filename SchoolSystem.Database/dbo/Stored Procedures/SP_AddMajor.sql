﻿CREATE Proc [dbo].[SP_AddMajor]
@NameAr nvarchar(50),
@NameEn nvarchar(50),
@QualificationId int,
@IsDeleted bit,
@Id int 
as
if(@Id=0 )
begin
insert into [dbo].[Major] (NameAr,NameEn,QualificationId,IsDeleted)  values(@NameAr,@NameEn,@QualificationId,@IsDeleted)  
end
else
begin
update[dbo].[Major] set NameAr=@NameAr,
NameEn=@NameEn,
QualificationId=@QualificationId,
IsDeleted=@IsDeleted

where Id=@Id
end