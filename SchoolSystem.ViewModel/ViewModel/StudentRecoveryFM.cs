﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class StudentRecoveryFM
    {
        public int Id { get; set; }
        public DateTime VoucherDate { get; set; }
        public int BranchId { get; set; }
        public int EducationYearId { get; set; }
        public int StudentId { get; set; }
        public decimal RecoveryValue { get; set; }
        public decimal TotalAfterVAT { get; set; }
        public decimal VAT { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
