﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class BusFM
    {
        public int Id { get; set; }
        public string BusName { get; set; }
        public string BusNo { get; set; }
        public int Capacity { get; set; }
        public int EducationYearId{ get; set; }
        public int BranchId{ get; set; }
        public int DriverId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
