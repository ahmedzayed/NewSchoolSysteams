﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class ClassFM
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public int EducationYearId { get; set; }
        public int BranchId { get; set; }
        public int EducationalGradeId { get; set; }
        public int StudentCount { get; set; }
        public int StageId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
