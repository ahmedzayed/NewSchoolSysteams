﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class RegistrationStudentVM
    {
        public int Id { get; set; }
        public string StuNameAr { get; set; }
        public string ParentNameAr { get; set; }
        public string GrandfatherNameAr { get; set; }
        public string FamilyNameAr { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
        public decimal RegisterFees { get; set; }
        public decimal StudyFees { get; set; }
        public decimal AdditionalFees { get; set; }
        public decimal DiscountPercentage { get; set; }
        public string RecordNumber { get; set; }
        public int EnrollmentStatus { get; set; }
        public decimal TransportationFees { get; set; }
        public decimal PreviousFees { get; set; }
        public bool GenderType { get; set; }
        public string FatherPhone { get; set; }
        public string MotherPhone { get; set; }
        public string BranchName { get; set; }
        public string EducationYearName { get; set; }
        public string ClassName { get; set; }
        public string BusName { get; set; }
        public string NationalityName { get; set; }
        public string StageName { get; set; }
        public string    EducationalGradeName { get; set; }
        public string DistrictName { get; set; }
        public string BusSubscriptionFeesTypeName { get; set; }
        public decimal Total { get; set; }
        public DateTime BirtheDate { get; set; }
        public decimal ResidualValue { get; set; }
    }
}
