﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class EmployeeAttendenceVM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string BranchName { get; set; }
        public string EducationalYearName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
