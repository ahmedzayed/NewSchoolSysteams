﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class RegisterationEmployeeFM
    {

        public int Id { get; set; }
        public string EmployeeNameAr { get; set; }
        public string FatherNameAr { get; set; }
        public string GrandFatherNameAr { get; set; }
        public string FamilyNameAr { get; set; }
        public int BranchId { get; set; }
        public int EducationYearId { get; set; }
        public int JobId { get; set; }
        public int TeacherStatusId { get; set; }
        public int MaterialId { get; set; }
        public int QualificationId { get; set; }
        public int MajorId { get; set; }
        public string RecordNo { get; set; }
        public DateTime HireDate { get; set; }
       
        public int NationalityId { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }

        public Array List { get; set; }
    }
}
