﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class ClassVM
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public string EducationYearName { get; set; }
        public string BranchName{ get; set; }
        public string EducationalGradeName { get; set; }
        public int StudentCount { get; set; }
        public bool IsDeleted { get; set; }
    }
}
