﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class BranchDrop
    {
        public int BranchId { get; set; }
        public string NameAr { get; set; }
    }
}
