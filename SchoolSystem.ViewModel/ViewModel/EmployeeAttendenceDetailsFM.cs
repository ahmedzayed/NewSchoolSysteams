﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class EmployeeAttendenceDetailsFM
    {
        public int Id { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public int EmployeeId { get; set; }
        public bool Attended { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Datetime { get; set; }
        public int BranchId { get; set; }
    }
}
