﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class StudientAttachementVM
    {
        public int Id { get; set; }
        public int StudientId { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }
}
