﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class UserVM
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
