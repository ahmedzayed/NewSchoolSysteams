﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class PayrollDataVM
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string PayrollItemName { get; set; }
        public decimal Value { get; set; }
    }
}
