﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class RegisterStudentAttachVM
    {
        public List<RegistrationStudentFM> RegistrationStudentFM { get; set; }
        public List<StudientAttachementVM> StudientAttachementVM { get; set; }
        public string Descroption1 { get; set; }
        public string Descroption2 { get; set; }
        public string Descroption3 { get; set; }
        public string Descroption4 { get; set; }
        public string Descroption5 { get; set; }
    }
}
