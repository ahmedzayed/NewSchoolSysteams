﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class SchoolExpensesVM
    {
        public int Id { get; set; }
        public string DocNo { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string ItemName { get; set; }
        public string DeliveredTo { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        public bool IsDeleted { get; set; }
    }
}
        