﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class StudentAttendenceVM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string BranchName { get; set; }
        public string EducationalYearName { get; set; }
        public string ClassName { get; set; }
        public string StageName { get; set; }
        public bool IsDeleted { get; set; }
        public string EducationalGradeName { get; set; }
    }
}
