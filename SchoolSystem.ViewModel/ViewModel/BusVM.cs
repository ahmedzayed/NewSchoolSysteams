﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class BusVM
    {
        public int Id { get; set; }
        public string BusName { get; set; }
        public string BusNo { get; set; }
        public int Capacity { get; set; }
        public string EducationYearName { get; set; }
        public string BranchName { get; set; }
        public string DriverName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
