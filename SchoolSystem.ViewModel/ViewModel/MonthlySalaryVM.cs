﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class MonthlySalaryVM
    {   
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string BranchName { get; set; }
        public string EducationalYearName { get; set; }
        public string EmployeeName { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public decimal NetSalary { get; set; }
    }
}
