﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class RegistrationEmployeeVM
    {
        public int Id { get; set; }
        public string EmployeeNameAr { get; set; }
        public string FatherNameAr { get; set; }
        public string GrandFatherNameAr { get; set; }
        public string FamilyNameAr { get; set; }
        public string BranchName { get; set; }
        public string EducationYearName { get; set; }
        public string JobName { get; set; }
        public string TeacherStatusName { get; set; }
        public string MaterialName { get; set; }
        public string QualificationName { get; set; }
        public string MajorName { get; set; }
        public string RecordNo { get; set; }
        public DateTime HireDate { get; set; }
        public string NationalityName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }

    }
}
