﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class StudentAttendenceFM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Branch_Id { get; set; }
        public int EducationalYear_Id { get; set; }
        public int Class_Id { get; set; }
        public int Stage_Id { get; set; }
        public bool IsDeleted { get; set; }
        public int EducationalGrade_Id { get; set; }
    }
}
