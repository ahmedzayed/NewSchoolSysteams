﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class StudentAttendenceDetailsFM
    {
        public int Id { get; set; }
        public int StudentAttendence_Id { get; set; }
        public int Student_Id { get; set; }
        public bool Attended { get; set; }
        public bool IsDeleted { get; set; }
    }
}
