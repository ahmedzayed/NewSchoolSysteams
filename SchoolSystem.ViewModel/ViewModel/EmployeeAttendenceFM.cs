﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class EmployeeAttendenceFM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int BranchId { get; set; }
        public int EducationalYearId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
