﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class StudentRecoveryVM
    {
        public int Id { get; set; }
        public DateTime VoucherDate { get; set; }
        public string BranchName { get; set; }
        public string EducationYearName { get; set; }
        public string StudentName { get; set; }
        public decimal RecoveryValue { get; set; }
        public decimal TotalAfterVAT { get; set; }
        public decimal VAT { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
