﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class EducationFeeFM
    {
        public int Id { get; set; }
        public decimal EducationFeesAmount { get; set; }
        public decimal RegistrationFeesAmount { get; set; }
        public int BranchId { get; set; }
        public int EducationYearId { get; set; }
        public int EducationalGradeId { get; set; }
        public int StageId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
