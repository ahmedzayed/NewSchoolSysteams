﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class RegistrationStudentFM
    {
        public int Id { get; set; }
        public string StuNameAr { get; set; }
        public string ParentNameAr { get; set; }
        public string GrandfatherNameAr { get; set; }
        public string FamilyNameAr { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
        public decimal RegisterFees { get; set; }
        public decimal StudyFees { get; set; }
        public decimal AdditionalFees { get; set; }
        public decimal DiscountPercentage { get; set; }
        public string RecordNumber { get; set; }
        public int EnrollmentStatus { get; set; }
        public decimal TransportationFees { get; set; }
        public decimal PreviousFees { get; set; }
        public bool GenderType { get; set; }
        public string FatherPhone { get; set; }
        public string MotherPhone { get; set; }
        public int BranchId { get; set; }
        public int EducationYearId { get; set; }
        public int ClassId { get; set; }
        public int BusId { get; set; }
        public int NationalityId { get; set; }
        public int StageId { get; set; }
        public int EducationalGradeId { get; set; }
        public int DistrictId { get; set; }
        public int BusSubscriptionFeesTypeId { get; set; }
        public decimal Total { get; set; }
        public DateTime BirtheDate { get; set; }
        public DateTime RegistrationDate { get; set; }
        public decimal ResidualValue { get; set; }
        //public string Descroption1 { get; set; }
        //public string Descroption2 { get; set; }
        //public string Descroption3 { get; set; }
        //public string Descroption4 { get; set; }
        //public string Descroption5 { get; set; }
    }
}
