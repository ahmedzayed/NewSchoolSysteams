﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class BusSubscriptionFeeVM
    {
        public int Id { get; set; }
        public string EducationYearName{ get; set; }
        public string BranchName { get; set; }
        public string SubscriptionTypeName { get; set; }
        public decimal  Fees { get; set; }
        public bool IsDeleted { get; set; } 
    }
}
