﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class BusDistrictFM
    {
        public int Id { get; set; }
        public int BusId { get; set; }
        public int DistrictId { get; set; }
        public bool IsDeleted { get; set; }
        public int BranchId { get; set; }
    }
}
