﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
  public  class MonthlySalaryFM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int BranchId { get; set; }
        public int EducationalYearId { get; set; }
        public int EmployeeId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public decimal NetSalary { get; set; }
    }
}
