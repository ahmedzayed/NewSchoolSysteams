﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
 public   class BusSubscriptionFeeFM
    {
        public int Id { get; set; }
        public int EducationYearId { get; set; }
        public int BranchId { get; set; }
        public int SubscriptionType { get; set; }
        public decimal Fees { get; set; }
        public bool IsDeleted { get; set; }
    }
}
