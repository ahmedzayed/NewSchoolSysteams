﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSystem.ViewModel.ViewModel
{
   public class   MonthlySalaryDetailsVM
    {
        public int Id { get; set; }
        public int MonthlySalaryId { get; set; }
        public int PayrollItemId { get; set; }
        public decimal Value { get; set; }
    }
}
