﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class StudentPaymentController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: StudentPayment
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId =Convert.ToInt32( Request.Cookies["BranchId"].Value);

            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "StudentPaymentController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "StudentPaymentController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "StudentPaymentController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {

                var model = "SP_GetAllStudentPayment".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<StudentPaymentVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetFees(int StudentId)
        {


            var model = "SP_GetFeesByStudentId".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)).AsList<RegistrationStudentVM>();
            return PartialView("_GetFees",model.FirstOrDefault());
        }
        public ActionResult Details(int StudentId=0)
        {


            var model = "SP_GetAllStudentPayment1".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)).AsList<RegistrationStudentVM>();
            return PartialView("Search", model);
        }

        public ActionResult Create()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);

            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_StudentDropDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.QualificationList = lst;
            StudentPaymentFM Obj = new StudentPaymentFM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(StudentPaymentFM model, decimal ResidualValue=0)
        {
            if(ResidualValue >= model.PaymentValue)
            {
                int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
                int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
                model.VAT = model.TotalAfterVAT - model.PaymentValue;
                var data = "SP_AddStudentPayment".ExecuParamsSqlOrStored(false, 
                    "BranchId".KVP(BranchId),
                    "YearId".KVP(YearId),
                    "Notes".KVP(model.Notes),
                    "PaymentValue".KVP(model.PaymentValue),
                    "StudentId".KVP(model.StudentId),
                    "TotalAfterVAT".KVP(model.TotalAfterVAT),
                    "VAT".KVP(model.VAT),
                    "VoucherDate".KVP(model.VoucherDate),
                    "IsDeleted".KVP(false),
                    "Id".KVP(model.Id)).AsNonQuery();
                if (data != 0)
                {


                    TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                    return RedirectToAction("Index1");

                }
                else
                    TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";
                return RedirectToAction("Index1");
            }
            TempData["success"] = "<script>alert(' القيمة المدفوعة اكبر من القيمة المستحقة  ');</script>";


            return RedirectToAction("Create");

        }




        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectStudentPaymentById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SettingVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            SettingVM obj = new SettingVM();
            obj.Id = id;
            return PartialView("~/Views/StudentPayment/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteStudentPayment".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}