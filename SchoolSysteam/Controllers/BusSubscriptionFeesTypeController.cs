﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class BusSubscriptionFeesTypeController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: BusSubscriptionFeesType
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "BusSubscriptionFeesTypeController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "BusSubscriptionFeesTypeController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "BusSubscriptionFeesTypeController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {

                var model = "SP_GetAllBusSubscriptionFeesType".ExecuParamsSqlOrStored(false).AsList<BusSubscriptionFeesTypeVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            BusSubscriptionFeesTypeVM Obj = new BusSubscriptionFeesTypeVM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(BusSubscriptionFeesTypeVM model)
        {

            model.NameEn = model.NameAr;
            var data = "SP_AddBusSubscriptionFeesType".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr), "NameEn".KVP(model.NameEn),"Value".KVP(model.Value), "IsDeleted".KVP(false), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectBusSubscriptionFeesTypeById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<BusSubscriptionFeesTypeVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            BusSubscriptionFeesTypeVM obj = new BusSubscriptionFeesTypeVM();
            obj.Id = id;
            return PartialView("~/Views/BusSubscriptionFeesType/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteBusSubscriptionFeesType".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}