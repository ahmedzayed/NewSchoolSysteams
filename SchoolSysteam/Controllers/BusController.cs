﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class BusController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int GroupId =Convert.ToInt32( Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "BusController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "BusController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "BusController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                var model = "SP_GetAllBus".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<BusVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            BusFM Obj = new BusFM();
            int BranchId =Convert.ToInt32( Request.Cookies["BranchId"].Value);
            var lst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_DriverDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.DriverList = lst;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(BusFM model)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int yearId = Convert.ToInt32(Request.Cookies["YearId"].Value);

            var data = "SP_AddBus".ExecuParamsSqlOrStored(false, "BusName".KVP(model.BusName), "BusNo".KVP(model.BusNo),
                "Capacity".KVP(model.Capacity),
                "EducationYearId".KVP(yearId),
                "BranchId".KVP(BranchId),
                "DriverId".KVP(model.DriverId),
                "IsDeleted".KVP(false),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);

            var lst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_DriverDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.DriverList = lst;

            var model = "SP_SelectBusById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<BusFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            BusFM obj = new BusFM();
            obj.Id = id;
            return PartialView("~/Views/Bus/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteBus".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}