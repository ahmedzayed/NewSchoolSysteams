﻿using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class BranchController : Controller
    {
        // GET: Branch
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllBranch".ExecuParamsSqlOrStored(false).AsList<SettingVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            SettingVM Obj = new SettingVM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(SettingVM model)
        {

            model.NameEn = model.NameAr;
            var data = "SP_AddBranch".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr), "NameEn".KVP(model.NameEn), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectBranchById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SettingVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            SettingVM obj = new SettingVM();
            obj.Id = id;
            return PartialView("~/Views/Branch/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteBranch".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}