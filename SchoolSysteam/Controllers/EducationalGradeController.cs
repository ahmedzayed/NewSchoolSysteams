﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class EducationalGradeController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "EducationalGradeController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "EducationalGradeController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "EducationalGradeController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {

                var model = "SP_GetAllEducationalGrade".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<EducationalGradeVM>();

                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            EducationalGradeFM Obj = new EducationalGradeFM();

            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = lst;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(EducationalGradeFM model)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);

            model.NameEn = model.NameAr;
            var data = "SP_AddEducationalGrade".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr), 
                "NameEn".KVP(model.NameEn),
                "StageId".KVP(model.StageId),
                "BranchId".KVP(BranchId),
                "YearId".KVP(YearId),
                "IsDeleted".KVP(false),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = lst;
            var model = "SP_SelectEducationalGradeById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<EducationalGradeFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            EducationalGradeFM obj = new EducationalGradeFM();
            obj.Id = id;
            return PartialView("~/Views/EducationalGrade/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteEducationalGrade".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}