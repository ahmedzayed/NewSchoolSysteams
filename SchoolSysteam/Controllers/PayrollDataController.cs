﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class PayrollDataController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: PayrollData
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "PayrollDataController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "PayrollDataController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "PayrollDataController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {

                var model = "SP_GetAllPayrollData".ExecuParamsSqlOrStored(false).AsList<PayrollDataVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            int BranchId =Convert.ToInt32( Request.Cookies["BranchId"].Value);
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_EmployeeDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EmployeeList = lst;

            var PayrollItem =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            PayrollItem.AddRange("Sp_PayrollItemDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.PayrollItemList = PayrollItem;
            PayrollDataFM Obj = new  PayrollDataFM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create( PayrollDataFM model)
        {

           
            var data = "SP_AddPayrollData".ExecuParamsSqlOrStored(false, "EmployeeId".KVP(model.EmployeeId),
                "PayrollItemId".KVP(model.PayrollItemId),
                "Value".KVP(model.Value),
               
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_EmployeeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EmployeeList = lst;

            var PayrollItem =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            PayrollItem.AddRange("Sp_PayrollItemDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.PayrollItemList = PayrollItem;
            var model = "SP_SelectPayrollDataById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<PayrollDataFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
             PayrollDataFM obj = new  PayrollDataFM();
            obj.Id = id;
            return PartialView("~/Views/PayrollData/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeletePayrollData".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}