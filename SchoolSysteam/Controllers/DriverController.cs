﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class DriverController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "DriverController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "DriverController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "DriverController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {


                var model = "SP_GetAllDriver".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<DriverVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            DriverVM Obj = new DriverVM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(DriverVM model)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);

            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);


            model.NameEn = model.NameAr;
            var data = "SP_AddDriver".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr),
                "NameEn".KVP(model.NameEn),
                "BranchId".KVP(BranchId),
                "YearId".KVP(YearId),
                "IsDeleted".KVP(false),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectDriverById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<DriverVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            DriverVM obj = new DriverVM();
            obj.Id = id;
            return PartialView("~/Views/Driver/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteDriver".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}