﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class EducationFeeController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {


            int BranchId =Convert.ToInt32( Request.Cookies["BranchId"].Value);
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "EducationFeeController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "EducationFeeController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "EducationFeeController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                var model = "SP_GetAllEducationFee".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<EducationFeeVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            EducationFeeFM Obj = new EducationFeeFM();
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_EducationalGradeDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = lst;
            var StageList =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            StageList.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = StageList;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(EducationFeeFM model)
        {

            model.BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            model.EducationYearId = Convert.ToInt32( Request.Cookies["YearId"].Value);

            var data = "SP_AddEducationFee".ExecuParamsSqlOrStored(false, "EducationYearId".KVP(model.EducationYearId),
                "BranchId".KVP(model.BranchId),
                "EducationalGradeId".KVP(model.EducationalGradeId), 
                "EducationFeesAmount".KVP(model.EducationFeesAmount), 
                "RegistrationFeesAmount".KVP(model.RegistrationFeesAmount), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);


            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_EducationalGradeDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = lst;
            var StageList =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            StageList.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = StageList;
            var model = "SP_SelectEducationFeeById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<EducationFeeFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            SettingVM obj = new SettingVM();
            obj.Id = id;
            return PartialView("~/Views/EducationFee/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteEducationFee".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}