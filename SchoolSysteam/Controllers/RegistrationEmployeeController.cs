﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class RegistrationEmployeeController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: RegistrationEmployee
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "RegistrationEmployeeController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "RegistrationEmployeeController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "RegistrationEmployeeController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                var model = "SP_GetAllRegistrationEmployee".ExecuParamsSqlOrStored(false, "BrachId".KVP(BranchId)).AsList<RegistrationEmployeeVM>();
                return View(model);
            }
            else
            {
                return View();
            }

        }


        public ActionResult Create()
        {
            RegisterationEmployeeFM Obj = new RegisterationEmployeeFM();

            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            var Stlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stlst.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stlst;

            var Edlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Edlst.AddRange("Sp_EducationalGradeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = Edlst;
            var JobList = new List<SelectListItem>  ();
            JobList.AddRange("Sp_JobDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.JobList = JobList;

            var TeacherStatusList =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TeacherStatusList.AddRange("Sp_TeacherStatusDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TeacherStatusList = TeacherStatusList;

            var MaterialList =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            MaterialList.AddRange("Sp_MaterialDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MaterialList = MaterialList;

            var QualificationList =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            QualificationList.AddRange("Sp_QualificationDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.QualificationList = QualificationList;

            var MajorList = new List<SelectListItem>   ();
            MajorList.AddRange("Sp_MajorDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MajorList = MajorList;

            var NationalityList = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            NationalityList.AddRange("Sp_NationalityDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = NationalityList;


            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(Array StageList,string Address,string Email,string EmployeeNameAr
            ,string FamilyNameAr,string FatherNameAr, string GrandFatherNameAr ,DateTime HireDate,int JobId=0,
            int MajorId=0,string MobileNo="" ,int MaterialId=0, int NationalityId=0,string  Notes="",string PhoneNumber="", 
            int QualificationId=0,string RecordNo="", int TeacherStatusId=0,int Id=0)
        {
            
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            
            var data = "SP_AddRegistrationEmployee".ExecuParamsSqlOrStored(false,
                "Address".KVP(Address),
                "BranchId".KVP(BranchId),
                "YearId".KVP(YearId),
                "Email".KVP(Email),
                "EmployeeNameAr".KVP(EmployeeNameAr),
                "FamilyNameAr".KVP(FamilyNameAr),
                "FatherNameAr".KVP(FatherNameAr),
                "GrandFatherNameAr".KVP(GrandFatherNameAr),
                "HireDate".KVP(HireDate),
                "IsDeleted".KVP(false),
                "JobId".KVP(JobId),
                "MajorId".KVP(MajorId),
                "MaterialId".KVP(MaterialId),
                "MobileNo".KVP(MobileNo),
                "NationalityId".KVP(NationalityId),
                "Notes".KVP(Notes),
                "PhoneNumber".KVP(PhoneNumber),
                "QualificationId".KVP(QualificationId),
                "RecordNo".KVP(RecordNo),
                "TeacherStatusId".KVP(TeacherStatusId),
                
                "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                foreach (var item in StageList)
                {
                    var model = "SP_GetAllRegistrationEmployee".ExecuParamsSqlOrStored(false, "BrachId".KVP(BranchId)).AsList<RegistrationEmployeeVM>().LastOrDefault();

                    
                    var Stage = "SP_AddEmployeeWorkStage".ExecuParamsSqlOrStored(false, "EmployeeId".KVP(model.Id),
                        "StageId".KVP(item),
                       
                        "Id".KVP(model.Id)).AsNonQuery();

                }

                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult StageList()
        {
            var model = "SP_GetAllStage".ExecuParamsSqlOrStored(false).AsList<SettingVM>();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult getMajorList(int id)
        {
            var MajorList =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            MajorList.AddRange("Sp_MajorDrop2".ExecuParamsSqlOrStored(false, "Id".KVP(id), "StageId".KVP(id)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MajorList = MajorList;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(MajorList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectRegistrationEmployeeById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SettingVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            SettingVM obj = new SettingVM();
            obj.Id = id;
            return PartialView("~/Views/RegistrationEmployee/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteRegistrationEmployee".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}