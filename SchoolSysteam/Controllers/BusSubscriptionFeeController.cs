﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class BusSubscriptionFeeController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId =Convert.ToInt32( Request.Cookies["BranchId"].Value);

            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "BusSubscriptionFeeController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "BusSubscriptionFeeController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "BusSubscriptionFeeController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                //var model = "SP_GetAllBusSubscriptionFee".ExecuParamsSqlOrStored(false,"BanchId".KVP(BranchId)).AsList<BusSubscriptionFeeVM>();
                var model = "SP_GetAllBusSubscriptionFeesType".ExecuParamsSqlOrStored(false).AsList<BusSubscriptionFeesTypeVM>();

                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            BusSubscriptionFeeFM Obj = new BusSubscriptionFeeFM();
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_BusSubscriptionFeeTypeDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusSubscriptionFeeTypeList = lst;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(BusSubscriptionFeeFM model)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);


            var data = "SP_AddBusSubscriptionFee".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId),
                "EducationYearId".KVP(YearId),
                "Fees".KVP(model.Fees),
                "SubscriptionType".KVP(model.SubscriptionType),
                "IsDeleted".KVP(false),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        //public ActionResult Edit(int id = 0)
        //{
        //    var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
        //    lst.AddRange("Sp_BusSubscriptionFeeTypeDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
        //    {
        //        Text = s.NameAr,
        //        Value = s.Id.ToString(),
        //        Selected = s.Id == 0 ? true : false
        //    }).ToList());
        //    ViewBag.BusSubscriptionFeeTypeList = lst;

        //    var model = "SP_SelectBusSubscriptionFeeById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<BusSubscriptionFeeFM>();
        //    if (model == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View("Create", model.FirstOrDefault());
        //}

        [HttpPost]
        public ActionResult Edit (int FeeType = 0, int Value = 0,string NameAr="")
        {



            var model = "SP_SelectBusSubscriptionFeeByFee".ExecuParamsSqlOrStored(false, "FeeType".KVP(FeeType)).AsList<BusSubscriptionFeeFM>().FirstOrDefault();
            int Id = FeeType;
            if (model == null)
            {
                 Id = 0;
            }
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);


            var data1 = "SP_AddBusSubscriptionFeesType".ExecuParamsSqlOrStored(false, "NameAr".KVP(NameAr), "NameEn".KVP(NameAr), "Value".KVP(Value), "IsDeleted".KVP(false), "Id".KVP(FeeType)).AsNonQuery();

            var data = "SP_AddBusSubscriptionFee".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId),
                "EducationYearId".KVP(YearId),
                "Fees".KVP(Value),
                "SubscriptionType".KVP(FeeType),
                "IsDeleted".KVP(false),
                "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BusSubscriptionFeeFM obj = new BusSubscriptionFeeFM();
            obj.Id = id;
            return PartialView("~/Views/BusSubscriptionFee/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteBusSubscriptionFee".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}