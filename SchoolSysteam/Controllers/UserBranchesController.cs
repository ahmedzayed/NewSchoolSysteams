﻿using Microsoft.AspNet.Identity;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class UserBranchesController : Controller
    {
        // GET: UserBranches
        public ActionResult Index1()

        {
          

            


            return View();
        }
        private long GenerateId()
        {
            byte[] buffer = Guid.NewGuid().ToByteArray();
            return BitConverter.ToInt16(buffer, 1);
        }

        public ActionResult Search()
        {

            var model = "SP_GetUser".ExecuParamsSqlOrStored(false).AsList<UserVM>();
            return View(model);

        }

        public ActionResult Create()
        {
            BranchOfUserFM Obj = new BranchOfUserFM();
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_UserDrop".ExecuParamsSqlOrStored(false).AsList<UserDrop>().Select(s => new SelectListItem
            {
                Text = s.UserName,
                Value = s.Id.ToString(),
                Selected = s.Id == "0" ? true : false
            }).ToList());
            ViewBag.UserList = lst;


            var lst2 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst2.AddRange("Sp_BranchDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = lst2;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(BranchOfUserFM model)
        {

            var data = "SP_AddUserBranches".ExecuParamsSqlOrStored(false, "UserId".KVP(model.UserId), "BranchId".KVP(model.BranchId), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }

        public ActionResult GetBranches(string Id)
        {
           var model= "SP_SelectBranchesOfUser".ExecuParamsSqlOrStored(false,"Id".KVP(Id)).AsList<UserOfStoreVM>();

            return PartialView("~/Views/UserBranches/GetBranches.cshtml", model);
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            UserOfStoreVM obj = new UserOfStoreVM();
            obj.Id = id;
            return PartialView("~/Views/UserBranches/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteBranchesOfUser".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }



        public ActionResult BranchOfUser()
        {

            string Id = User.Identity.GetUserId();
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("SP_BranchOfUserDrop".ExecuParamsSqlOrStored(false,"Id".KVP(Id)).AsList<BranchDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.BranchId.ToString(),
                Selected = s.BranchId == 0 ? true : false
            }).ToList());
            ViewBag.BranchesList = lst;

            BranchOfUserFM obj = new BranchOfUserFM();

            return View(obj);
        }
        [HttpPost]
        public ActionResult BranchOfUser(BranchOfUserFM model)
        {

        if(model.BranchId != 0)
            {
                HttpCookie cookie = new HttpCookie("BranchId","0");
                cookie.Value = model.BranchId.ToString() ;
                Response.SetCookie(cookie);
                Response.Cookies.Add(cookie);
                cookie.Expires.AddDays(10);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("BranchOfUser");
            }

        }
    }
}