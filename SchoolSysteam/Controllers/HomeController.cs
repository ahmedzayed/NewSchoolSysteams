﻿using Microsoft.AspNet.Identity;
using SchoolSysteam.Entitys;
using SchoolSysteam.Models;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class HomeController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();
        ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            try
            {
                var BranchId = Request.Cookies["BranchId"].Value;
                var model = "SP_GetEducationYearDef".ExecuParamsSqlOrStored(false).AsList<EducationYearVM>();

                HttpCookie cookieyear = new HttpCookie("YearId", "0");
                cookieyear.Value = model.LastOrDefault().Id.ToString();
                Response.SetCookie(cookieyear);
                Response.Cookies.Add(cookieyear);
                cookieyear.Expires.AddDays(10);
                var YearId = Request.Cookies["YearId"].Value;


                var userid = User.Identity.GetUserId();
                var Users = db.Users.Include(u => u.Roles).Where(a => a.Id == userid).FirstOrDefault();

                var Roleid = string.Join(",", Users.Roles.Select(r => r.RoleId));
                var Groupid = un.AspNetRoles.Where(a => a.Id == Roleid.ToString()).Select(a => a.GroupsId).FirstOrDefault();


                HttpCookie cookieGroup = new HttpCookie("GroupId", "0");
                cookieGroup.Value = Groupid.ToString();
                Response.SetCookie(cookieGroup);
                Response.Cookies.Add(cookieGroup);
                cookieGroup.Expires.AddDays(10);

                return View();
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}