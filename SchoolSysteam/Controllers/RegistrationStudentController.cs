﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class RegistrationStudentController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: RegistrationStudent
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "RegistrationStudentController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "RegistrationStudentController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "RegistrationStudentController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                var model = "SP_GetAllRegistrationStudent".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<RegistrationStudentVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            var Stlst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stlst.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stlst;

            var Edlst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Edlst.AddRange("Sp_EducationalGradeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = Edlst;


            var Cllst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClassDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = Cllst;
            var Nalst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nalst.AddRange("Sp_NationalityDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nalst;

            var Buslst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Buslst.AddRange("Sp_BusDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<BusDrop>().Select(s => new SelectListItem
            {
                Text = s.BusName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusList = Buslst;

            

                 var Bussublst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Bussublst.AddRange("Sp_BusSubscriptionFeesTypeDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusSubscriptionFeesTypeList = Bussublst;

            var Dislst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Dislst.AddRange("Sp_DistrictDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.DistrictList = Dislst;

            RegistrationStudentFM Obj = new RegistrationStudentFM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult GetEducationalGradeList(int id)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            var Edlst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Edlst.AddRange("Sp_EducationalGradeDrop2".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId),"StageId".KVP(id)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = Edlst;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Edlst, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetClassesByEduGradeIdList(int id)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);

            var Cllst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClassDrop2".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "EduGradeId".KVP(id)).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = Cllst;

            return Json(Cllst, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetEducationFees(int EducationalGradeId)
        {

            var J = "SP_GetEducationFees".ExecuParamsSqlOrStored(false, "EducationalGradeId".KVP(EducationalGradeId)).AsList<EducationFeeFM>().FirstOrDefault();


            return Json(J, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetTransportationFees(int BusSubscriptionFeesTypeId)
        {
            decimal Feestrasport = 0;
            var J = "SP_GetTransportationFees".ExecuParamsSqlOrStored(false, "BusSubscriptionFeesTypeId".KVP(BusSubscriptionFeesTypeId)).AsList<BusSubscriptionFeesTypeVM>().FirstOrDefault();

            
                Feestrasport = J.Value;
           
            return Json(Feestrasport, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(RegistrationStudentFM model,string Descroption1,
            string Descroption2,string Descroption3 ,string Descroption4 ,string Descroption5 ,int Id1=0 ,int Id2=0,int Id3=0,int Id4=0 ,int Id5=0)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);


            var data = "SP_AddRegisterStudient".ExecuParamsSqlOrStored(false,
                "AdditionalFees".KVP(model.AdditionalFees),
                "Address".KVP(model.Address),
                "BirtheDate".KVP(model.BirtheDate),
                "BranchId".KVP(BranchId),
                "BusId".KVP(model.BusId),
                 "BusSubscriptionFeesTypeId".KVP(model.BusSubscriptionFeesTypeId),
                 "ClassId".KVP(model.ClassId),
                 "DiscountPercentage".KVP(model.DiscountPercentage),
                 "DistrictId".KVP(model.DistrictId),
                 "EducationalGradeId".KVP(model.EducationalGradeId),
                    "YearId".KVP(YearId),
                    "EnrollmentStatus".KVP(model.EnrollmentStatus),
                    "FamilyNameAr".KVP(model.FamilyNameAr),
                    "FatherPhone".KVP(model.FatherPhone),
                    "GrandfatherNameAr".KVP(model.GrandfatherNameAr),
                    "MotherPhone".KVP(model.MotherPhone),
                     "NationalityId".KVP(model.NationalityId),
                     "Notes".KVP(model.Notes),
                     "ParentNameAr".KVP(model.ParentNameAr),
                     "PreviousFees".KVP(model.PreviousFees),
                     "RecordNumber".KVP(model.RecordNumber),
                     "RegisterFees".KVP(model.RegisterFees),
                     "RegistrationDate".KVP(model.RegistrationDate),
                   "StageId".KVP(model.StageId),
                   "StudyFees".KVP(model.StudyFees),
                   "StuNameAr".KVP(model.StuNameAr),
                   "Total".KVP(model.Total),
                   "TransportationFees".KVP(model.TransportationFees),
                    "IsDeleted".KVP(false),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                
                var LastRec = "SP_GetAllRegistrationStudent".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<RegistrationStudentVM>().LastOrDefault();
                int StudentId = LastRec.Id;
                if (model.Id != 0)
                {
                    StudentId = model.Id;
                }
                StudientAttachementVM SS = new StudientAttachementVM();
                if (Request.Files.Count > 0)
                {


                    int i=0;
                    foreach (var item in Request.Files)
                    {
                        string Des="";
                        int Id = 0;
                        if (i == 0)
                        {
                            Des = Descroption1;
                           
                            Id =(int) Id1;
                        }
                        else if (i == 1)
                        {
                            Des = Descroption2;
                            Id = (int)Id2;

                        }
                        else if (i == 2)
                        {
                            Des = Descroption3;
                            Id = (int)Id3;
                        }
                        else if (i == 3)
                        {
                            Des = Descroption4;
                            Id = (int)Id4;
                        }
                        else if (i == 4)
                        {
                            Des = Descroption5;
                            Id = (int)Id5;
                        }
                        var file = Request.Files[i];
                        i++;
                        var fileNameWithExtension = Path.GetFileName(file.FileName);
                        var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                        string RootPath = Server.MapPath("~/Images/");
                        if (!Directory.Exists(RootPath))
                        {
                            Directory.CreateDirectory(RootPath);
                        }
                        var unique = new Guid();
                        var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                        file.SaveAs(path);
                        SS.Image = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);
                        SS.StudientId = StudentId;

                        SS.Description = Des;
                        var data1 = "SP_AddStudientAttachement".ExecuParamsSqlOrStored(false, "StudientId".KVP(SS.StudientId),
                            "Image".KVP(SS.Image),
                            "Description".KVP(SS.Description),"Id".KVP(Id)).AsNonQuery();

                    }

                  


                }
                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");
            
            


          

        }


        public ActionResult Edit(int id = 0)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            var Stlst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stlst.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stlst;

            var Edlst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Edlst.AddRange("Sp_EducationalGradeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = Edlst;


            var Cllst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClassDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = Cllst;
            var Nalst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nalst.AddRange("Sp_NationalityDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nalst;

            var Buslst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Buslst.AddRange("Sp_BusDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<BusDrop>().Select(s => new SelectListItem
            {
                Text = s.BusName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusList = Buslst;



            var Bussublst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Bussublst.AddRange("Sp_BusSubscriptionFeesTypeDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusSubscriptionFeesTypeList = Bussublst;

            var Dislst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Dislst.AddRange("Sp_DistrictDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.DistrictList = Dislst;


            ViewBag.Edit = 1;


            var model = "SP_SelectRegistrationStudentById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<RegistrationStudentFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        public ActionResult EditAttachment(int Id = 0)
        {

            var model = "SP_GetAttachmentById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<StudientAttachementVM>();

            return PartialView("EditAttcahment", model);
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            RegistrationStudentFM obj = new RegistrationStudentFM();
            obj.Id = id;
            return PartialView("~/Views/RegistrationStudent/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteRegistrationStudent".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}