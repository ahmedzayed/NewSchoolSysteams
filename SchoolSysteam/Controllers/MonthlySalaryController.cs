﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class MonthlySalaryController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: MonthlySalary
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "MonthlySalaryController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "MonthlySalaryController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "MonthlySalaryController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                var model = "SP_GetAllMonthlySalary".ExecuParamsSqlOrStored(false).AsList<MonthlySalaryVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }

        public ActionResult PartialMonthSal(int EmployeeId = 0)
        {
            var model = "SP_GetAllPayrollDataByEmp".ExecuParamsSqlOrStored(false,"EmployeeId".KVP(EmployeeId)).AsList<PayrollDataVM2>();
            return PartialView(model);
        }

        public ActionResult Create()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_EmployeeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EmployeeList = lst;

         
            MonthlySalaryFM Obj = new MonthlySalaryFM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(PayValVM ListItem, int EmployeeId=0 ,int Month=0,int Year=0,decimal NetSalary=0)
        {
          

            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);

            int Id = 0;
            var data = "SP_AddMonthlySalary".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId),"YearId".KVP(YearId),"EmployeeId".KVP(EmployeeId),
                "Monthe".KVP(Month),
                "Year".KVP(Year),
                
                "Date".KVP(DateTime.Now),
                "NetSalary".KVP(NetSalary),

                "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                var LastRow = "SP_GetAllMonthlySalary".ExecuParamsSqlOrStored(false).AsList<MonthlySalaryVM>().LastOrDefault();


                int i = 0;
                foreach (var item in ListItem.PayItem)
                {

                  var Va= ListItem.ValueItem[i];
                        var model = "SP_AddMonthlySalaryDetails".ExecuParamsSqlOrStored(false, "MonthlySalaryId".KVP(LastRow.Id), "PayrollItemId".KVP(item),
                       "Value".KVP(Va),
                      "Id".KVP(Id)).AsNonQuery();
                    i++;
                        
                    
                    
                
                }





                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_EmployeeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EmployeeList = lst;

            var PayrollItem =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            PayrollItem.AddRange("Sp_PayrollItemDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.PayrollItemList = PayrollItem;
            var model = "SP_SelectMonthlySalaryById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<MonthlySalaryFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            MonthlySalaryFM obj = new MonthlySalaryFM();
            obj.Id = id;
            return PartialView("~/Views/MonthlySalary/Delete.cshtml", obj);
        }


        [HttpPost]
        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteMonthlySalary".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}