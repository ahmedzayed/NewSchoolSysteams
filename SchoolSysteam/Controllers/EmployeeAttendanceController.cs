﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class EmployeeAttendanceController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: EmployeeAttendance
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "EmployeeAttendanceController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "EmployeeAttendanceController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "EmployeeAttendanceController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                var model = "SP_GetAllEmployeeAttendence".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "YearId".KVP(YearId)).AsList<EmployeeAttendenceVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }
        public ActionResult Create()
        {
            EmployeeAttendenceVM obj = new EmployeeAttendenceVM();
            return View(obj);
        }
        public ActionResult GetEmployee()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            var model = "SP_GetAllRegistrationEmployee".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<RegistrationEmployeeVM>();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult AddAttanduce(Array StudentList, DateTime Date)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            int Id = 0;
            var model = "SP_GetEmployeeAttacndeceByDate".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId), "EducationyearId".KVP(YearId),"Date".KVP(Date)).AsList<EmployeeAttendenceFM>();
            if (model.Count == 0)
            {
                Id = 0;
                var data = "SP_AddEmployeeAttanduce".ExecuParamsSqlOrStored(false,
                    "Date".KVP(Date)
                    , "BranchId".KVP(BranchId), "YearId".KVP(YearId)
                    ).AsNonQuery();
                if (data != 0)
                {
                    var model1 = "SP_GetAllEmployeeAttendence1".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "EducationyearId".KVP(YearId)).AsList<EmployeeAttendenceFM>();

                    foreach (var item in StudentList)
                    {

                        var Attan = "SP_AddEmployeeAttanduceDetails".ExecuParamsSqlOrStored(false,
                            "EmployeeAttendenceId".KVP(model1.LastOrDefault().Id),
                            "EmployeeId".KVP(item),
                            "Attended".KVP(false), "Datetime".KVP(Date),
                            "BranchId".KVP(BranchId), "Id".KVP(Id)).AsNonQuery();

                    }
                    TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                    return RedirectToAction("Index1");

                }
                else
                    TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

                return RedirectToAction("Index1");
            }
            else
            {
                Id = model.FirstOrDefault().Id;


                foreach (var item in StudentList)
                {
                    var data = "SP_GetAllEmployeeAttendence1".ExecuParamsSqlOrStored(false, "HeaderId".KVP(Id), "EmpId".KVP(item)).AsList<EmployeeAttendenceDetailsFM>();
                    if (data.Count == 0)
                    {
                        var Attan = "SP_AddEmployeeAttanduceDetails".ExecuParamsSqlOrStored(false,
                          "EmployeeAttendenceId".KVP(Id),
                          "EmployeeId".KVP(item),
                          "Attended".KVP(false), "Datetime".KVP(Date),
                          "BranchId".KVP(BranchId), "Id".KVP(Id)).AsNonQuery();

                    }
                }
                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");



            }


        }

        public ActionResult Details(int Id = 0)
        {
            var model = "SP_GetAllEmpAttanduceById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EmployeeAttendenceDetailsVM>();

            return View(model);
        }


        public ActionResult Edit(int Id = 0)
        {
            var model = "SP_GetAllEmpAttanduceById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EmployeeAttendenceDetailsVM>();

            return View(model);
        }

        public ActionResult DeleteAttandce(Array StudentList)
        {
            foreach (var item in StudentList)
            {

                var data = "SP_DeleteEmpAttandce".ExecuParamsSqlOrStored(false, "Id".KVP(item)).AsNonQuery();
                if (data != 0)
                {
                    return RedirectToAction("Index1");
                }
                else
                    return RedirectToAction("Index1");
            }
            return RedirectToAction("Index1");


        }


        public ActionResult Report()
        {

            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            var Stlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stlst.AddRange("Sp_BranchDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = Stlst;

            var Edlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Edlst.AddRange("Sp_EducationYearDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalYearList = Edlst;


           
            EmployeeAttendenceFM Obj = new EmployeeAttendenceFM();

            return View(Obj);
        }

        public ActionResult GetReport(DateTime DateTo, DateTime DateFrom, int BranchId = 0, int YearId=0)
        {
            var model = "SP_GetEmployeeReport".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<EmployeeAttendenceDetailsVM>();
            return PartialView(model.Where(a => a.Datetime >= DateFrom && a.Datetime <= DateTo).ToList());
        }
    }
}