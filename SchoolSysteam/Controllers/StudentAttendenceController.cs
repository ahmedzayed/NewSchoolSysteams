﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class StudentAttendenceController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: StudentAttendence
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "StudentAttendenceController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "StudentAttendenceController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "StudentAttendenceController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                var model = "SP_GetAllStudentAttendence".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "EducationyearId".KVP(YearId)).AsList<StudentAttendenceVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }

        public ActionResult Create()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            var Stlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } } ;
            Stlst.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stlst;

            var Edlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } } ;
            Edlst.AddRange("Sp_EducationalGradeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = Edlst;


            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } } ;
            Cllst.AddRange("Sp_ClassDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = Cllst;
            StudentAttendenceFM Obj = new StudentAttendenceFM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult AddAttanduce(Array StudentList, DateTime Date ,int StageId=0 ,int EducationGardId=0,int ClassId = 0)
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            int Id = 0;
            var model = "SP_GetStudentAttacndeceByDate".ExecuParamsSqlOrStored(false, "Date".KVP(Date)).AsList<StudentAttendenceFM>();
            if (model.Count == 0)
            {
                Id = 0;
                var data = "SP_AddStudentAttanduce".ExecuParamsSqlOrStored(false,
                    "Date".KVP(Date)
                    , "StageId".KVP(StageId), "EducationGardId".KVP(EducationGardId),
                    "ClassId".KVP(ClassId),"BranchId".KVP(BranchId),"YearId".KVP(YearId), "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    var model1 = "SP_GetAllStudentAttendence".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "EducationyearId".KVP(YearId)).AsList<StudentAttendenceVM>();

                    foreach (var item in StudentList)
                    {
                        var Attan = "SP_AddStudentAttanduceDetails".ExecuParamsSqlOrStored(false, 
                            "StudentAttendence_Id".KVP(model1.LastOrDefault().Id),
                            "Student_Id".KVP(item),
                            "Attended".KVP(false),"Datetime".KVP(Date),
                            "ClassId".KVP(ClassId),"Id".KVP(Id)).AsNonQuery();

                    }
                    TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                    return RedirectToAction("Index1");

                }
                else
                    TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

                return RedirectToAction("Index1");
            }
            else
            {
                Id = model.FirstOrDefault().Id;

               
                    foreach (var item in StudentList)
                    {
                    var data = "SP_GetAllStudentAtta".ExecuParamsSqlOrStored(false, "StudentId".KVP(item), "HeaderId".KVP(Id)).AsList<StudentAttendenceDetailsFM>();
                    if (data.Count == 0)
                    {
                        var Attan = "SP_AddStudentAttanduceDetails".ExecuParamsSqlOrStored(false, "StudentAttendence_Id".KVP(Id), "Student_Id".KVP(item), "Attended".KVP(true), "Id".KVP(Id)).AsNonQuery();
                    }
                    }
                    TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                    return RedirectToAction("Index1");

                
                
            }
            

        }

        
        public ActionResult GetStudent(int ClassId=0)
        {
            var model = "SP_GetAllRegistrationStudentByClass".ExecuParamsSqlOrStored(false, "ClassId".KVP(ClassId)).AsList<RegistrationStudentVM>();

            return PartialView(model);
        }


        public ActionResult Details(int Id=0)
        {
            var model = "SP_GetAllAttanduceById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<StudentAttendenceDetailsVM>();

            return View(model);
        }

        public ActionResult Edit(int Id = 0)
        {
            var model = "SP_GetAllAttanduceById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<StudentAttendenceDetailsVM>();

            return View(model);
        }

        public ActionResult DeleteAttandce(Array StudentList)
        {
            foreach (var item in StudentList)
            {

                var data = "SP_DeleteAttandce".ExecuParamsSqlOrStored(false, "Id".KVP(item)).AsNonQuery();
                if (data != 0)
                {
                    return RedirectToAction("Index1");
                }
                else
                    return RedirectToAction("Index1");
            }
            return RedirectToAction("Index1");


        }
        public ActionResult Report()
        {

            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
            int YearId = Convert.ToInt32(Request.Cookies["YearId"].Value);
            var Stlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stlst.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stlst;

            var Edlst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Edlst.AddRange("Sp_EducationalGradeDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = Edlst;


            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClassDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = Cllst;
            StudentAttendenceFM Obj = new StudentAttendenceFM();

            return View(Obj);
        }
        public ActionResult GetReport(DateTime DateTo,DateTime DateFrom,int ClassId=0)
        {
            var model = "SP_GetStudentReport".ExecuParamsSqlOrStored(false, "ClassId".KVP(ClassId)).AsList<StudentAttendenceDetailsVM>();
            return PartialView(model.Where(a=>a.Datetime >=DateFrom && a.Datetime<=DateTo).ToList());
         }
    }
}