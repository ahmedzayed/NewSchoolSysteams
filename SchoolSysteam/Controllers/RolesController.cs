﻿
using SchoolSysteam.Entitys;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SchoolSysteam.Models.RolesVM;

namespace SchoolSysteam.Controllers
{
    public class RolesController : Controller
    {
        // GET: Roles

        DB_A40639_SchoolSystemEntities db = new DB_A40639_SchoolSystemEntities();

        public ActionResult AppContollerRoles(string keysearch, int? page)
        {
            var appCnt = db.AppControllers.Where(x => keysearch == null || x.ControllerName.Contains(keysearch)).ToList();
            var cnt = appCnt.ToList();

            if (Request.IsAjaxRequest())
            {
                return PartialView("_AppConrollerList", cnt);
            }


            return View(cnt);
        }
        public ActionResult GetActions(int id)
        {

            var ac = db.AppActions.Where(c => c.ControllerId == id).ToList();
            return PartialView("_ControllerActions", ac);
        }

        public ActionResult EditActionRoles(int id)
        {
            var acs = db.AppActions.Find(id);

            return PartialView("_EditActionRoles", acs);
        }
        [HttpPost]
        public ActionResult EditActionRoles(int Id, string Description)
        {
            var ac = db.AppActions.Find(Id);
            ac.Id = Id;
            ac.Description = Description;
            db.AppActions.AddOrUpdate(ac);
            db.SaveChanges();

            return RedirectToAction("AppContollerRoles");
        }


        public ActionResult AddUserRoles()
        {


            ViewBag.UserId = new SelectList(db.AspNetUsers.ToList(), "Id", "UserName");
            ViewBag.RoleId = new SelectList(db.AspNetRoles.ToList(), "Id", "Name");




            return View();
        }

        [HttpPost]
        public ActionResult AddUserRoles(AspNetUserRole model)
        {

            try
            {

                var Role = db.AspNetUserRoles.Where(a => a.UserId == model.UserId && a.RoleId == model.RoleId).FirstOrDefault();
                if (Role == null)
                {
                    AspNetUserRole S = new AspNetUserRole()
                    {
                        UserId = model.UserId,
                        RoleId = model.RoleId

                    };
                    db.AspNetUserRoles.Add(S);
                    db.SaveChanges();
                    TempData["msg"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                }
                else
                {
                    Role.RoleId = model.RoleId;
                    Role.UserId = model.UserId;
                    db.AspNetUserRoles.Attach(Role);
                    db.SaveChanges();
                    TempData["msg"] = "<script>alert(' تم الاضافه بنجاح');</script>";

                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' تم اصافة الصلاحية للمستخدم من قبل  ');</script>";

            }




            return RedirectToAction("AddUserRoles");
        }



        public ActionResult AddRoles()
        {


            ViewBag.GroupsId = new SelectList(db.Groups.ToList(), "Id", "Name");




            return View();
        }
        [HttpPost]
        public ActionResult AddRoles(AspNetRole model)
        {
            try
            {

                var id = db.AspNetRoles.ToList().Select(a => a.Id).LastOrDefault();
                int Id = Convert.ToInt32(id) + 1;
                AspNetRole S = new AspNetRole()
                {
                    GroupsId = model.GroupsId,
                    Name = model.Name,
                    Id = Id.ToString(),
                };
                db.AspNetRoles.Add(S);
                db.SaveChanges();
                TempData["msg"] = "<script>alert(' تم الاضافه بنجاح');</script>";


                return RedirectToAction("AddRoles");
            }
            catch
            {
                TempData["msg"] = "<script>alert(' تم اصافة الصلاحية  من قبل  ');</script>";
                return RedirectToAction("AddRoles");

            }
        }
        #region Groups
        public ActionResult Groups()
        {
            var gp = db.Groups.ToList();
            return View(gp);
        }

        public ActionResult NewGroup()
        {

            return View("AddGroup", new NewGroup
            {
                Roles = db.AppActions.Select(role => new RolesCheckBox
                {
                    Id = role.Id,
                    Name = role.Description,
                    IsChecked = false
                }).ToList()
            });
        }

        [HttpPost]
        public ActionResult NewGroup(NewGroup form)
        {
            var grp = new Group();


            grp.Name = form.Name;
            grp.Description = form.Description;

            Group g = new Group()
            {
                Name = grp.Name
            };
            db.Groups.Add(g);

            db.SaveChanges();
            foreach (var item in form.Roles.Where(a => a.IsChecked == true))
            {
                RolesGroup RG = new RolesGroup
                {
                    Groups_Id = g.Id,
                    Roles_Id = item.Id

                };
                db.RolesGroups.Add(RG);
                db.SaveChanges();
            }

            var gps = db.Groups.ToList();
            return RedirectToAction("Groups");

        }



        public ActionResult EditGroup(int id)
        {
            var grp = db.Groups.FirstOrDefault(x => x.Id == id);
            if (grp == null)
                return HttpNotFound();

            EditGroup edg = new EditGroup();
            edg.Name = grp.Name;
            edg.Description = grp.Description;
            edg.Roles = new List<RolesCheckBox>();
            bool cases = true;
            foreach (var role in db.AppActions.ToList())
            {

                if (role.RolesGroups.Where(a => a.Groups_Id == id).Select(a => a.Roles_Id).FirstOrDefault() == role.Id)
                {
                    cases = true;
                }
                else
                {
                    cases = false;
                }


                var listItem = new RolesCheckBox()
                {
                    Id = role.Id,
                    Name = role.Description,
                    IsChecked = cases
                };



                edg.Roles.Add(listItem);


            }
            return PartialView("_EditGroup", edg);

        }

        [HttpPost]
        public ActionResult EditGroup(int id, EditGroup form)
        {
            var gr = db.Groups.FirstOrDefault(x => x.Id == id);


            gr.Name = form.Name;
            gr.Description = form.Description;


            db.SaveChanges();
            var Oldadate = db.RolesGroups.Where(a => a.Groups_Id == id).ToList();
            db.RolesGroups.RemoveRange(Oldadate);
            db.SaveChanges();
            foreach (var item in form.Roles.Where(a => a.IsChecked == true))
            {
                RolesGroup R = new RolesGroup()
                {
                    Groups_Id = id,
                    Roles_Id = item.Id
                };
                db.RolesGroups.Add(R);
            }
            db.SaveChanges();
            return RedirectToAction("Groups");

        }



        public ActionResult DeleteGroup(int id)
        {
            var us = db.Groups.FirstOrDefault(x => x.Id == id);
            if (us == null)
                return HttpNotFound();

            db.Groups.Remove(us);
            db.SaveChanges();
            var rolesGroup = db.RolesGroups.Where(a => a.Groups_Id == id).ToList();
            db.RolesGroups.RemoveRange(rolesGroup);
            db.SaveChanges();
            var data = db.Groups.ToList();
            return RedirectToAction("Groups", data);
        }




        private IList<Role> syncGroupsRoles(IList<RolesCheckBox> checkRols, IList<Role> Roles)
        {
            var selectedRoles = new List<Role>();
            foreach (var item in db.Roles.ToList())
            {
                var checkboxe = checkRols.Single(x => x.Id == item.Id);
                checkboxe.Name = item.RoleName;
                if (checkboxe.IsChecked)
                    selectedRoles.Add(item);
            }
            foreach (var toAdd in selectedRoles.Where(t => !Roles.Contains(t)))
            {
                Roles.Add(toAdd);
            }

            foreach (var toremove in Roles.Where(t => !selectedRoles.Contains(t)).ToList())
            {
                Roles.Remove(toremove);
            }
            return Roles;
        }
        #endregion  

    }
}