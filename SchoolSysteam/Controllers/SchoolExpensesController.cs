﻿using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class SchoolExpensesController : Controller
    {
        // GET: SchoolExpenses
        public ActionResult Index1()
        {

            return View();
        }

        public ActionResult Search()
        {
            
            var model = "SP_GetAllSchoolExpenses".ExecuParamsSqlOrStored(false).AsList<SchoolExpensesVM>();
            return View(model);
        }

        public ActionResult Create()
        {
            SchoolExpensesVM obj = new SchoolExpensesVM();

            return View(obj);
        }
        [HttpPost]
        public ActionResult Create(SchoolExpensesVM model)
        {

            var data = "SP_AddSchoolExpenses".ExecuParamsSqlOrStored(false, "DeliveredTo".KVP(model.DeliveredTo),
                "ExpenseDate".KVP(model.ExpenseDate),
                "ItemName".KVP(model.ItemName),
                "Price".KVP(model.Price),
                "Quantity".KVP(model.Quantity),
                "Total".KVP(model.Total),
                "IsDeleted".KVP(false),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectSchoolExpensesById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SchoolExpensesVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            SchoolExpensesVM obj = new SchoolExpensesVM();
            obj.Id = id;
            return PartialView("~/Views/SchoolExpenses/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteSchoolExpenses".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Reports()
        {

            return View();
        }

        public ActionResult GetExReports(DateTime StartDate , DateTime EndDate)
        {

            var model = "SP_GetAllSchoolExpenses".ExecuParamsSqlOrStored(false).AsList<SchoolExpensesVM>();
            return PartialView(model.Where(a=>a.ExpenseDate >=StartDate && a.ExpenseDate<=EndDate).ToList());
        }

        public ActionResult Revenuesandexpenses()
        {
            return View();
        }
        
        public ActionResult GetRevenues(DateTime StartDate, DateTime EndDate)
        {

            var model = "SP_GetAllStudentPaymentReport".ExecuParamsSqlOrStored(false).AsList<StudentPaymentVM>();
            var data = model.Where(a => a.VoucherDate >= StartDate && a.VoucherDate <= EndDate).ToList();
            ViewBag.Revenues = data.Sum(a => a.TotalAfterVAT);
            return PartialView(data);
        }
        public ActionResult Getexpenses(DateTime StartDate, DateTime EndDate)
        {

            var model = "SP_GetAllSchoolExpenses".ExecuParamsSqlOrStored(false).AsList<SchoolExpensesVM>();
            var data = model.Where(a => a.ExpenseDate >= StartDate && a.ExpenseDate <= EndDate).ToList();
            ViewBag.Total = data.Sum(a => a.Total);
            return PartialView(data);
        }



    }
}