﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class BusDistrictController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            int GroupId = Convert.ToInt32(Request.Cookies["GroupId"].Value);
            var Actions = un.AppActions.Where(a => a.AppController.ControllerName == "BusDistrictController" && a.ActionName == "Index1").Select(a => a.Id).FirstOrDefault();
            var ActionsAddEdit = un.AppActions.Where(a => a.AppController.ControllerName == "BusDistrictController" && a.ActionName == "Create").Select(a => a.Id).FirstOrDefault();
            var ActionsDelete = un.AppActions.Where(a => a.AppController.ControllerName == "BusDistrictController" && a.ActionName == "Delete").Select(a => a.Id).FirstOrDefault();

            var IndexRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == Actions).Count();
            ViewBag.EditAddRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsAddEdit).Count();
            ViewBag.DeleteRole = un.RolesGroups.Where(a => a.Groups_Id == GroupId && a.Roles_Id == ActionsDelete).Count();

            if (IndexRole != 0)
            {
                int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);
                var model = "SP_GetAllBusDistrict".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<BusDistrictVM>();
                return View(model);
            }
            else
            {
                return View();
            }
        }


        public ActionResult Create()
        {
            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);

            BusDistrictFM Obj = new BusDistrictFM();
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_BusDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId)).AsList<BusDrop>().Select(s => new SelectListItem
            {
                Text = s.BusName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusList = lst;

            var lst2 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst2.AddRange("Sp_DistrictDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.Sp_DistrictList = lst2;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(BusDistrictFM model)
        {

            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);

            var data = "SP_AddBusDistrict".ExecuParamsSqlOrStored(false, "BusId".KVP(model.BusId),
                "DistrictId".KVP(model.DistrictId), "BranchId".KVP(BranchId) , "IsDeleted".KVP(false),"Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            int BranchId = Convert.ToInt32(Request.Cookies["BranchId"].Value);

            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_BusDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<BusDrop>().Select(s => new SelectListItem
            {
                Text = s.BusName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusList = lst;

            var lst2 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst2.AddRange("Sp_DistrictDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.Sp_DistrictList = lst2;
            var model = "SP_SelectBusDistrictById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<BusDistrictFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            BusDistrictFM obj = new BusDistrictFM();
            obj.Id = id;
            return PartialView("~/Views/BusDistrict/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteBusDistrict".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}