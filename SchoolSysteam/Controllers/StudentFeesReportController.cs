﻿using SchoolSysteam.Entitys;
using SchoolSystem.Core;
using SchoolSystem.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolSysteam.Controllers
{
    public class StudentFeesReportController : Controller
    {
        DB_A40639_SchoolSystemEntities un = new DB_A40639_SchoolSystemEntities();

        // GET: StudentFeesReport
        public ActionResult Index1()
        {
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_BranchDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = lst;
            var lst2 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst2.AddRange("Sp_EducationYearDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationYearList = lst2;

            var lst3 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst3.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = lst3;


            var lst4 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst4.AddRange("Sp_EducationalGradeDrop3".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = lst4;



            var lst5 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst5.AddRange("Sp_ClassDrop3".ExecuParamsSqlOrStored(false).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = lst5;
            return View();
        }

        public ActionResult Search(int BranchId=0,int YearId=0, int StageId=0,int EducationalGradeId=0,int ClassId=0)
        {
            if (BranchId == 0 && YearId == 0 && StageId == 0 && YearId == 0 && EducationalGradeId == 0 && ClassId == 0)
            {
                var model = "SP_GetAllStudentFeesReportList".ExecuParamsSqlOrStored(false).AsList<RegistrationStudentVM>();
                return PartialView(model);
            }
            else
            {
                var model = "SP_GetAllStudentFeesReport".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "YearId".KVP(YearId), "StageId".KVP(StageId), "EducationalGradeId".KVP(EducationalGradeId), "ClassId".KVP(ClassId)).AsList<RegistrationStudentVM>();
                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult GetEducationalGradeList(int id)
        {
            var Edlst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Edlst.AddRange("Sp_EducationalGradeDrop4".ExecuParamsSqlOrStored(false,  "StageId".KVP(id)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = Edlst;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Edlst, JsonRequestBehavior.AllowGet);
        }
       

        public ActionResult getBusList(int id)
        {
            var lst3 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst3.AddRange("Sp_BusDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(id)).AsList<BusDrop>().Select(s => new SelectListItem
            {
                Text = s.BusName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusList = lst3;
            return Json(lst3, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getStudentList(int id)
        {
            var lst3 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst3.AddRange("Sp_StudentDropDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(id)).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StudentList = lst3;
            return Json(lst3, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetClassesByEduGradeIdList(int id)
        {

            var Cllst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClassDrop4".ExecuParamsSqlOrStored(false,  "EduGradeId".KVP(id)).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = Cllst;

            return Json(Cllst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ParentPhoneStudentReport()
        {
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_BranchDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = lst;
            var lst2 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst2.AddRange("Sp_EducationYearDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationYearList = lst2;

            var lst3 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst3.AddRange("Sp_StageDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = lst3;


            var lst4 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst4.AddRange("Sp_EducationalGradeDrop3".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationalGradeList = lst4;



            var lst5 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst5.AddRange("Sp_ClassDrop3".ExecuParamsSqlOrStored(false).AsList<ClassDrop>().Select(s => new SelectListItem
            {
                Text = s.ClassName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ClassList = lst5;
            return View();
        }

        public ActionResult ParentPhoneStudentReportSearch(int BranchId = 0, int YearId = 0, int StageId = 0, int EducationalGradeId = 0, int ClassId = 0)
        {
            if (BranchId == 0 && YearId == 0 && StageId == 0 && YearId == 0 && EducationalGradeId == 0 && ClassId == 0)
            {
                var model = "SP_GetAllStudentFeesReportList".ExecuParamsSqlOrStored(false).AsList<RegistrationStudentVM>();
                return PartialView(model);
            }
            else
            {
                var model = "SP_GetAllStudentFeesReport".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "YearId".KVP(YearId), "StageId".KVP(StageId), "EducationalGradeId".KVP(EducationalGradeId), "ClassId".KVP(ClassId)).AsList<RegistrationStudentVM>();
                return PartialView(model);
            }
        }

        public ActionResult BusStudentReport()
        {
            int BranchId =Convert.ToInt32( Request.Cookies["BranchId"].Value);
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_BranchDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = lst;
            var lst2 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst2.AddRange("Sp_EducationYearDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationYearList = lst2;

            var lst3 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst3.AddRange("Sp_BusDrop2".ExecuParamsSqlOrStored(false).AsList<BusDrop>().Select(s => new SelectListItem
            {
                Text = s.BusName,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BusList = lst3;
            
            return View();
        }


        public ActionResult SearchBus(int BranchId = 0, int YearId = 0, int BusId = 0)
        {
            if (BranchId == 0 && YearId == 0 && BusId == 0 )
            {
                var model = "SP_GetAllStudentFeesReportList".ExecuParamsSqlOrStored(false).AsList<RegistrationStudentVM>();
                return PartialView(model);
            }
            else
            {
                var model = "SP_GetAllStudentBusReport".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "YearId".KVP(YearId), "BusId".KVP(BusId)).AsList<RegistrationStudentVM>();
                return PartialView(model);
            }
        }


        public ActionResult StudentPaymentStatement()
        {
            var lst =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst.AddRange("Sp_BranchDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = lst;
            var lst2 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst2.AddRange("Sp_EducationYearDrop".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationYearList = lst2;

            var lst3 =  new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            lst3.AddRange("Sp_StudentDrop2".ExecuParamsSqlOrStored(false).AsList<QualificationDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StudentList = lst3;

            return View();

        }

        public ActionResult StudentDetails(int StudentId=0)
        {
            if(StudentId !=0)
            {

                var model = "SP_StudentDetails".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)).AsList<RegistrationStudentVM>();
                return PartialView(model.FirstOrDefault());
            }
            else
            {
             RegistrationStudentVM model = new RegistrationStudentVM();
                return PartialView(model);
            }
        }

        public ActionResult StudentPayment(int StudentId = 0)
        {
            if (StudentId != 0)
            {

                var model = "SP_GetStudentPayment".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)).AsList<StudentPaymentVM>();
                return PartialView(model);
            }
            else
            {
               List<StudentPaymentVM> model = new List<StudentPaymentVM>();
                return PartialView(model);
            }
        }


    }
}