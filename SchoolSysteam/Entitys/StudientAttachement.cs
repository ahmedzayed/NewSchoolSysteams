//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolSysteam.Entitys
{
    using System;
    using System.Collections.Generic;
    
    public partial class StudientAttachement
    {
        public int Id { get; set; }
        public Nullable<int> StudientId { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    
        public virtual RegistrationStudent RegistrationStudent { get; set; }
    }
}
