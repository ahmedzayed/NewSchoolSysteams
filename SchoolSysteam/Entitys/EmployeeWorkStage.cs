//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolSysteam.Entitys
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeWorkStage
    {
        public int Id { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> StageId { get; set; }
    
        public virtual RegistrationEmployee RegistrationEmployee { get; set; }
        public virtual Stage Stage { get; set; }
    }
}
