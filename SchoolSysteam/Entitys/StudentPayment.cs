//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolSysteam.Entitys
{
    using System;
    using System.Collections.Generic;
    
    public partial class StudentPayment
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> VoucherDate { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> EducationYearId { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<decimal> PaymentValue { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public string Notes { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<decimal> TotalAfterVAT { get; set; }
    }
}
