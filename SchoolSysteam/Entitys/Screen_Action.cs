//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolSysteam.Entitys
{
    using System;
    using System.Collections.Generic;
    
    public partial class Screen_Action
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Screen_Action()
        {
            this.ActionPermations = new HashSet<ActionPermation>();
        }
    
        public int Id { get; set; }
        public Nullable<int> ActionId { get; set; }
        public Nullable<int> ScreenId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActionPermation> ActionPermations { get; set; }
        public virtual ActionTB ActionTB { get; set; }
        public virtual Screen Screen { get; set; }
    }
}
