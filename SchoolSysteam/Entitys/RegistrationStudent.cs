//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolSysteam.Entitys
{
    using System;
    using System.Collections.Generic;
    
    public partial class RegistrationStudent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RegistrationStudent()
        {
            this.StudentAttendenceDetails = new HashSet<StudentAttendenceDetail>();
            this.StudentRecoveryBatches = new HashSet<StudentRecoveryBatch>();
            this.StudientAttachements = new HashSet<StudientAttachement>();
        }
    
        public int Id { get; set; }
        public string StuNameAr { get; set; }
        public string ParentNameAr { get; set; }
        public string GrandfatherNameAr { get; set; }
        public string FamilyNameAr { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<decimal> RegisterFees { get; set; }
        public Nullable<decimal> StudyFees { get; set; }
        public Nullable<decimal> AdditionalFees { get; set; }
        public string RecordNumber { get; set; }
        public Nullable<int> EnrollmentStatus { get; set; }
        public Nullable<decimal> TransportationFees { get; set; }
        public Nullable<decimal> PreviousFees { get; set; }
        public string FatherPhone { get; set; }
        public string MotherPhone { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> EducationYearId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> BusId { get; set; }
        public Nullable<int> NationalityId { get; set; }
        public Nullable<int> StageId { get; set; }
        public Nullable<int> EducationalGradeId { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> BusSubscriptionFeesTypeId { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<System.DateTime> BirtheDate { get; set; }
        public Nullable<bool> GenderType { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public Nullable<decimal> DiscountPercentage { get; set; }
        public Nullable<decimal> ResidualValue { get; set; }
    
        public virtual Branch Branch { get; set; }
        public virtual Bus Bus { get; set; }
        public virtual BusSubscriptionFeesType BusSubscriptionFeesType { get; set; }
        public virtual Class Class { get; set; }
        public virtual District District { get; set; }
        public virtual EducationalGrade EducationalGrade { get; set; }
        public virtual Nationality Nationality { get; set; }
        public virtual Stage Stage { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentAttendenceDetail> StudentAttendenceDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentRecoveryBatch> StudentRecoveryBatches { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudientAttachement> StudientAttachements { get; set; }
    }
}
